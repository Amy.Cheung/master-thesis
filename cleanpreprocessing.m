%% Preprocessing EEG-data
%folders: EC, EO, Energy, Jeans, filt, ICA

%% Import Data EO and Epoching

clear, clc;

%cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles'; %for test data
cd '/Volumes/G-DRIVE mobile USB-C/EEG'; %for real data

eInfo = dir('*.vhdr'); %just take the .vhdr-files
eegData = {eInfo.name}; %files are listed in each column

[EEG, ALLEEG, CURRENTSET, LASTCOM, ALLCOM] = eeglab;


for i = 37:39 %1:length(eegData) %index for all the files
    
    a = eegData{i};
    %name = a(3:5); for Pilot data sets
    name = a(5:7); %-> for the real data: example "MPR_DTH" (take the 5:7)
    
    %importing original raw eeg data
    disp(i);
    %EEG = pop_loadbv('/Users/amycheung/Desktop/Masterarbeit/audiofiles', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]); %choosing 32 channels because don't need the last two; 33 = EKG; 34 = GSR
    EEG = pop_loadbv('/Volumes/G-DRIVE mobile USB-C/EEG', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
    EEG.setname = strcat(name, 'EEG'); %saved the imported data
    EEG = eeg_checkset( EEG );
    
    EEG = pop_rmdat( EEG, {'S 83'},[-0.5 189.5] ,0);
    EEG.setname= strcat(name, 'EO');
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1);%delete second 'boundary' marker
    EEG = eeg_checkset( EEG );
    %setting channel locations
    EEG=pop_chanedit(EEG, 'lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp','lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp');
    EEG = eeg_checkset( EEG );
    %EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO/');
    EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Volumes/G-DRIVE mobile USB-C/EEG/EO/');
    EEG = eeg_checkset( EEG );
    
end


% Import Data EC and Epoching

for i = 1:length(eegData)
    
    a = eegData{i};
    %name = a(3:5);for Pilot data sets
    name = a(5:7); % -> for the real data: example "MPR_DTH" (take the 5:7)
    
    disp(i);
    %EEG = pop_loadbv('/Users/amycheung/Desktop/Masterarbeit/audiofiles', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
    EEG = pop_loadbv('/Volumes/G-DRIVE mobile USB-C/EEG', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
    EEG.setname = strcat(name, 'EEG'); %saved the imported data
    EEG = eeg_checkset( EEG );
    
    EEG = pop_rmdat( EEG, {'S 81'},[-0.5 189.5] ,0);
    EEG.setname= strcat(name, 'EC');
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1);%delete second 'boundary' marker
    EEG = eeg_checkset( EEG );
    %setting channel locations
    EEG=pop_chanedit(EEG, 'lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp','lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp');
    EEG = eeg_checkset( EEG );
    %EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC/');
    EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Volumes/G-DRIVE mobile USB-C/EEG/EC/');
    EEG = eeg_checkset( EEG );
    
end


% Import Data Energy and Epoching

for i = 1:length(eegData)
    
    a = eegData{i};
    %name = a(3:5);
    name = a(5:7); % -> for the real data: example "MPR_DTH" (take the 5:7)
    
    %loading original eeg data again
    disp(i);
    %EEG = pop_loadbv('/Users/amycheung/Desktop/Masterarbeit/audiofiles', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
    EEG = pop_loadbv('/Volumes/G-DRIVE mobile USB-C/EEG', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
    EEG.setname = strcat(name, 'EEG'); %saved the imported data
    EEG = eeg_checkset( EEG );
    EEG = pop_rmdat( EEG, {'S 22'},[-0.5 373.5] ,0);
    EEG.setname= strcat(name, 'Energy');
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1); %delete second 'boundary' marker
    EEG = eeg_checkset( EEG );
    %setting channel locations
    EEG=pop_chanedit(EEG, 'lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp','lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp');
    EEG = eeg_checkset( EEG );
    %EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy/');
    EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Volumes/G-DRIVE mobile USB-C/EEG/Energy/');
    EEG = eeg_checkset( EEG ); %end file should contain 2 markers ('S 22' and 'S 52')
    
end


%Importing Data Jeans and Epoching

for i = 1:length(eegData)
    
    a = eegData{i};
    %name = a(3:5);
    name = a(5:7); %-> for the real data: example "MPR_DTH" (take the 5:7)
    
    %loading orginal eeg data again
    
    disp(i);
    %EEG = pop_loadbv('/Users/amycheung/Desktop/Masterarbeit/audiofiles', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
    EEG = pop_loadbv('/Volumes/G-DRIVE mobile USB-C/EEG', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
    
    EEG.setname = strcat(name, 'EEG'); %saved the imported data
    EEG = eeg_checkset( EEG );
    
    %EEG = pop_loadset('filename', o,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles');
    %EEG = eeg_checkset( EEG );
    
    %extracting epoch Jeans
    %EO marker = 'S 23'
    %trial duration ~ 413.8sec (adding additional 2sec for buffer when
    %filtering data)
    EEG = pop_rmdat( EEG, {'S 23'},[-0.5 414.3] ,0);
    EEG.setname= strcat(name, 'Jeans');
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1);%delete second 'boundary' marker
    EEG = eeg_checkset( EEG );
    %setting channel locations
    EEG=pop_chanedit(EEG, 'lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp','lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp');
    EEG = eeg_checkset( EEG );
    %EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/');
    EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Volumes/G-DRIVE mobile USB-C/EEG/Jeans/');
    EEG = eeg_checkset( EEG );
    
end


%% Referencing, Downsampling, Filtering (high, notch, low)

%b = struct('c', {'/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/', '/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy/','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC', '/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO/'});
b = struct('c', {'/Volumes/G-DRIVE mobile USB-C/EEG/Jeans/', '/Volumes/G-DRIVE mobile USB-C/EEG/Energy/','/Volumes/G-DRIVE mobile USB-C/EEG/EC', '/Volumes/G-DRIVE mobile USB-C/EEG/EO/'});

sfilt = struct('f', {'/Volumes/G-DRIVE mobile USB-C/EEG/filt/Jeans', '/Volumes/G-DRIVE mobile USB-C/EEG/filt/Energy', '/Volumes/G-DRIVE mobile USB-C/EEG/filt/EO', '/Volumes/G-DRIVE mobile USB-C/EEG/filt/EC'});

for i = 1:length(b)
    
    d = b(i).c;
    cd (d);
    
    eInfo = dir('*.set'); %just take the .set-files
    Epoch = {eInfo.name};
    
    for j = 1:length(Epoch)
        
        a = Epoch{j};
        name = a(1:5);
        
        EEG = pop_loadset('filename', a,'filepath', d);
        %EEG = eeg_checkset( EEG );
        %EEG = pop_reref( EEG, []); %rereferencing with averaging methode
        EEG = eeg_checkset( EEG );
        EEG = pop_resample( EEG, 250); %resampling datasets from 5000Hz to 250Hz (eeglab automatically lowpass filters new dataset to half the sampling rate)
        EEG.setname= strcat(name, 'r'); %rr = 'rereferenced' & 'resampled'
        EEG = eeg_checkset( EEG );
        
        
        %Filtering sampled data
        EEG = pop_eegfiltnew(EEG, 'locutoff',0.1,'plotfreqz',0);
        EEG = eeg_checkset( EEG );
        
        %notch filter (50Hz) Line Noise
        EEG = pop_eegfiltnew(EEG, 'locutoff',49.5,'hicutoff',50.5,'revfilt',1,'plotfreqz',0);
        EEG = eeg_checkset( EEG );
        
        %low-pass filtering
        EEG = pop_eegfiltnew(EEG, 'hicutoff',30, 'plotfreqz',0);
        EEG.setname= strcat(name, '_filt');
        EEG = eeg_checkset( EEG );
        
        EEG.setname= strcat(name, '_filt');
        %EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/filt');
        EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Volumes/G-DRIVE mobile USB-C/EEG/filt');
        EEG = eeg_checkset( EEG );
        
    end
    
    %dfilt = sfilt(i).f;
    %EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath', dfilt);
    %EEG = eeg_checkset( EEG );
end

%% ICA

cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/filt';
%cd '/Volumes/G-DRIVE mobile USB-C/EEG/filt';

icaInfo = dir('*.set'); %take filtered files from filt folder
icaDat = {icaInfo.name};

for j = 1:length(icaDat)
    
    a = icaDat{j};
    name = a(1:5);
    
    EEG = pop_loadset('filename', a,'filepath', '/Users/amycheung/Desktop/Masterarbeit/audiofiles/filt');
    %EEG = pop_loadset('filename', a,'filepath', '/Volumes/G-DRIVE mobile USB-C/EEG/filt');
    EEG = eeg_checkset( EEG );
    EEG = pop_runica(EEG, 'icatype', 'runica', 'extended',1,'interrupt','on');
    EEG = eeg_checkset( EEG );
    
    EEG.setname= strcat(name, '_ica');
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/ICA');
    %EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Volumes/G-DRIVE mobile USB-C/EEG/ICA');
    EEG = eeg_checkset( EEG );
    
end
% end

%Removing Components

cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/ICA';
%cd '/Volumes/G-DRIVE mobile USB-C/EEG/ICA';
icaInfo = dir('*.set'); %take filtered files from filt folder
icaDat = {icaInfo.name};

eeglab;

%Read in all the ICA files into the EEG lab gui
EEG = pop_loadset('filename', icaDat ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/ICA/');
%EEG = pop_loadset('filename', icaDat ,'filepath','/Volumes/G-DRIVE mobile USB-C/EEG/ICA/');
EEG = eeg_checkset( EEG );
[ALLEEG, EEG, CURRENTSET] = eeg_store(ALLEEG, EEG);
EEG = eeg_checkset(EEG);

eeglab redraw;

%save rejected Eye Components into a variable (-> under construction)
EEG.blink = input('Eye blink component');
EEG = pop_rejepoch( EEG, 3,0);
EEG = eeg_checkset( EEG );

%% Seperate into Groups

cd '/Volumes/G-DRIVE mobile USB-C/EEG/ICA';

cleanInfo = dir('*.set'); %take filtered files from filt folder
cleanDat = {cleanInfo.name};

cleanInfo2 = dir('*.fdt'); %take filtered files from filt folder
cleanDat2 = {cleanInfo2.name};



name = a(1:3);

for j = 1:length(cleanDat)
    
    fin = cleanDat{j};
    fin2 = cleanDat2{j};
    
    
    if fin(1:3) == x && fin(4:5) == 'EC'
        movefile(fin{j}, '/Volumes/G-DRIVE mobile USB-C/EEG/Clean/DPR/');
        movefile(fin2{j}, '/Volumes/G-DRIVE mobile USB-C/EEG/Clean/DPR');
    end
    
end


