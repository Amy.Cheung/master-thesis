%% Preprocessing EEG-data

clear, clc;

%% Epoching according to condition; Resampling; Filtering

b = struct('c', {'/Volumes/G-DRIVE mobile USB-C/EEG/ESJE/neu',...
    '/Volumes/G-DRIVE mobile USB-C/EEG/EEJS/neu'});

% cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles';
% 
% eInfo = dir('*.vhdr'); %just take the .vhdr-files
% eegData = {eInfo.name}; %files are listed in each column

for i = 1:length(b)
    
    d = b(i).c;
    cd (d);
    
    eInfo = dir('*.vhdr'); %just take the .vhdr-files
    eegData = {eInfo.name}; %files are listed in each column
    
    
    for z = 1:length(eegData) % Index for all the files
        
        a = eegData{z};
        name = a(1:7);
        
        % Importing original raw eeg data
        EEG = pop_loadbv(d, a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]); %choosing 32 channels because don't need the last two; 33 = EKG; 34 = GSR
        EEG.setname = strcat(name, 'EEG'); %saved the imported data
        EEG = eeg_checkset(EEG);
        
        
        % Extracting EO-epoch
        % EO marker = 'S 83'
        % Trial duration ~ 189sec + 0.2sec (delay)
        EEG = pop_rmdat( EEG, {'S 83'},[0 189.2] ,0);
        EEG = eeg_checkset( EEG );
        EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
        EEG = eeg_checkset( EEG );
        EEG = pop_editeventvals(EEG,'delete',1);%delete second 'boundary' marker
        EEG = eeg_checkset( EEG );
        
        % Setting channel locations
        EEG=pop_chanedit(EEG, 'lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp','lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp');
        EEG = eeg_checkset( EEG );
        
        % Resampling from 5000Hz to 250Hz (eeglab automatically lowpass filters new dataset to half the sampling rate)
        EEG = pop_resample( EEG, 250); 
        EEG = eeg_checkset( EEG );
        
        % Filtering Line Noise with eegplugin 'Cleanline'
        EEG = pop_cleanline(EEG, 'bandwidth',2,'chanlist',[1:32] ,'computepower',1,'linefreqs',50,'normSpectrum',0,'p',0.01,'pad',2,'plotfigures',0,'scanforlines',1,'sigtype','Channels','tau',100,'verb',1,'winsize',4,'winstep',1);
        EEG = eeg_checkset(EEG);
        
        % Filtering sampled data
        EEG = pop_eegfiltnew(EEG, 'locutoff',1,'plotfreqz',0);
        EEG = eeg_checkset( EEG );
        EEG = pop_eegfiltnew(EEG, 'hicutoff',15,'plotfreqz',0);
        EEG = eeg_checkset( EEG );

        % Saving
        EEG.setname= strcat(name, 'EO');
        EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Volumes/G-DRIVE mobile USB-C/EEG/eo/');
        EEG = eeg_checkset( EEG );
        
        
        
        % Loading data again
        EEG = pop_loadbv(d, a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
        EEG.setname = strcat(name, 'EEG'); %saved the imported data
        EEG = eeg_checkset( EEG );
        
        % Extracting epoch EC
        % EO marker = 'S 81'
        % Trial duration ~ 189sec + 0.2sec (delay)
        EEG = pop_rmdat( EEG, {'S 81'},[0 189.2] ,0);
        EEG = eeg_checkset( EEG );
        EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
        EEG = pop_editeventvals(EEG,'delete',1);%delete second 'boundary' marker
        EEG = eeg_checkset( EEG );
        
        % Setting channel locations
        EEG=pop_chanedit(EEG, 'lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp','lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp');
        EEG = eeg_checkset( EEG );
        
        % Resampling from 5000Hz to 250Hz (eeglab automatically lowpass filters new dataset to half the sampling rate)
        EEG = pop_resample( EEG, 250);
        EEG = eeg_checkset( EEG );
        
        % Filtering Line Noise with eegplugin 'Cleanline'
        EEG = pop_cleanline(EEG, 'bandwidth',2,'chanlist',[1:32] ,'computepower',1,'linefreqs',50,'normSpectrum',0,'p',0.01,'pad',2,'plotfigures',0,'scanforlines',1,'sigtype','Channels','tau',100,'verb',1,'winsize',4,'winstep',1);
        EEG = eeg_checkset(EEG);
        
        % Filtering sampled data
        EEG = pop_eegfiltnew(EEG, 'locutoff',1,'plotfreqz',0);
        EEG = eeg_checkset( EEG );
        EEG = pop_eegfiltnew(EEG, 'hicutoff',15,'plotfreqz',0);
        EEG = eeg_checkset( EEG );
        
        % Saving
        EEG.setname= strcat(name, 'EC');
        EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Volumes/G-DRIVE mobile USB-C/EEG/ec/');
        EEG = eeg_checkset( EEG );
        
        
        if i == 1
            
            EEG = pop_loadbv( b(1).c , a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
            EEG.setname = strcat(name, 'EEG');
            EEG = eeg_checkset( EEG );
            
            % Extracting EnergySE-epoch
            % Energy marker = 'S 22'
            % Trial duration ~ 373sec + 0.2sec (delay)
            EEG = pop_rmdat( EEG, {'S 22'},[0 374.4] ,0);
            EEG = eeg_checkset( EEG );
            EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
            EEG = eeg_checkset( EEG );
            EEG = pop_editeventvals(EEG,'delete',1); %delete second 'boundary' marker
            EEG = eeg_checkset( EEG );
            
            % Setting channel locations
            EEG=pop_chanedit(EEG, 'lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp','lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp');
            EEG = eeg_checkset(EEG);
            
            % Resampling from 5000Hz to 250Hz (eeglab automatically lowpass filters new dataset to half the sampling rate)
            EEG = pop_resample( EEG, 250);
            EEG = eeg_checkset( EEG );
            
            % Filtering Line Noise with eegplugin 'Cleanline'
            EEG = pop_cleanline(EEG, 'bandwidth',2,'chanlist',[1:32] ,'computepower',1,'linefreqs',50,'normSpectrum',0,'p',0.01,'pad',2,'plotfigures',0,'scanforlines',1,'sigtype','Channels','tau',100,'verb',1,'winsize',4,'winstep',1);
            EEG = eeg_checkset( EEG );

            % Filtering sampled data
            EEG = pop_eegfiltnew(EEG, 'locutoff',1,'plotfreqz',0);
            EEG = eeg_checkset( EEG );
            EEG = pop_eegfiltnew(EEG, 'hicutoff',15,'plotfreqz',0);
            EEG = eeg_checkset( EEG );
            
            %Saving
            EEG.setname= strcat(name, 'ES');
            EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Volumes/G-DRIVE mobile USB-C/EEG/energy/SE');
            EEG = eeg_checkset( EEG ); %end file should contain 2 markers ('S 22' and 'S 52')
            
            
            
            % Extracting JeansELF-epoch %
            EEG = pop_loadbv(d, a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
            EEG.setname = strcat(name, 'EEG'); %saved the imported data
            EEG = eeg_checkset( EEG );
       
            % Jeans marker = 'S 23'
            % Trial duration ~ 414.5sec + 0.2sec (delay)
            EEG = pop_rmdat( EEG, {'S 23'},[0 414.70] ,0);
            EEG = eeg_checkset( EEG );
            EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
            EEG = eeg_checkset( EEG );
            EEG = pop_editeventvals(EEG,'delete',1);%delete second 'boundary' marker
            EEG = eeg_checkset( EEG );
            
            % Setting channel locations
            EEG=pop_chanedit(EEG, 'lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp','lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp');
            EEG = eeg_checkset( EEG );
            
            % Resampling from 5000Hz to 250Hz (eeglab automatically lowpass filters new dataset to half the sampling rate)
            EEG = pop_resample( EEG, 250);
            EEG = eeg_checkset( EEG );
            
            % Filtering Line Noise with eegplugin 'Cleanline'
            EEG = pop_cleanline(EEG, 'bandwidth',2,'chanlist',[1:32] ,'computepower',1,'linefreqs',50,'normSpectrum',0,'p',0.01,'pad',2,'plotfigures',0,'scanforlines',1,'sigtype','Channels','tau',100,'verb',1,'winsize',4,'winstep',1);
            EEG = eeg_checkset( EEG );

            % Filtering sampled data
            EEG = pop_eegfiltnew(EEG, 'locutoff',1,'plotfreqz',0);
            EEG = eeg_checkset( EEG );
            EEG = pop_eegfiltnew(EEG, 'hicutoff',15,'plotfreqz',0);
            EEG = eeg_checkset( EEG );
            
            %Saving
            EEG.setname= strcat(name, 'JE');
            EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Volumes/G-DRIVE mobile USB-C/EEG/jeans/ELF');
            EEG = eeg_checkset( EEG );
            
            
        else
            
            % Extracting EnergyELF-epoch %

            EEG = pop_loadbv (b(2).c , a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
            EEG.setname = strcat(name, 'EEG');
            EEG = eeg_checkset( EEG );
            
            % Energy Elf marker = 'S 22'
            % Trial duration ~ 316.65sec + 0.2sec (delay)
            EEG = pop_rmdat( EEG, {'S 22'},[0 316.65] ,0);
            EEG = eeg_checkset( EEG );
            EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
            EEG = eeg_checkset( EEG );
            EEG = pop_editeventvals(EEG,'delete',1); %delete second 'boundary' marker
            EEG = eeg_checkset( EEG );
            
            % Setting channel locations
            EEG=pop_chanedit(EEG, 'lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp','lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp');
            EEG = eeg_checkset(EEG);
            
            % Resampling from 5000Hz to 250Hz (eeglab automatically lowpass filters new dataset to half the sampling rate)
            EEG = pop_resample( EEG, 250);
            EEG = eeg_checkset( EEG );
            
            % Filtering Line Noise with eegplugin 'Cleanline'
            EEG = pop_cleanline(EEG, 'bandwidth',2,'chanlist',[1:32] ,'computepower',1,'linefreqs',50,'normSpectrum',0,'p',0.01,'pad',2,'plotfigures',0,'scanforlines',1,'sigtype','Channels','tau',100,'verb',1,'winsize',4,'winstep',1);
            EEG = eeg_checkset( EEG );

            % Filtering sampled data
            EEG = pop_eegfiltnew(EEG, 'locutoff',1,'plotfreqz',0);
            EEG = eeg_checkset( EEG );
            EEG = pop_eegfiltnew(EEG, 'hicutoff',15,'plotfreqz',0);
            EEG = eeg_checkset( EEG );
            
            
            %Saving
            EEG.setname= strcat(name, 'EE');
            EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Volumes/G-DRIVE mobile USB-C/EEG/energy/ELF');
            EEG = eeg_checkset( EEG ); %end file should contain 2 markers ('S 22' and 'S 52')
            
            
            
            % Extracting JeansSE-epoch
            EEG = pop_loadbv( b(2).c , a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
            EEG.setname = strcat(name, 'EEG'); %saved the imported data
            EEG = eeg_checkset( EEG );
            
            % Jeans marker = 'S 23'
            % Trial duration ~ 385.6sec + 0.2sec (delay)
            EEG = pop_rmdat( EEG, {'S 23'},[0 385.8] ,0);
            
            EEG = eeg_checkset( EEG );
            EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
            EEG = eeg_checkset( EEG );
            EEG = pop_editeventvals(EEG,'delete',1);%delete second 'boundary' marker
            EEG = eeg_checkset( EEG );
            
            % Setting channel locations
            EEG=pop_chanedit(EEG, 'lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp','lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp');
            EEG = eeg_checkset( EEG );
            
            % Resampling from 5000Hz to 250Hz (eeglab automatically lowpass filters new dataset to half the sampling rate)
            EEG = pop_resample( EEG, 250);
            EEG = eeg_checkset( EEG );
            
            % Filtering Line Noise with eegplugin 'Cleanline'
            EEG = pop_cleanline(EEG, 'bandwidth',2,'chanlist',[1:32] ,'computepower',1,'linefreqs',50,'normSpectrum',0,'p',0.01,'pad',2,'plotfigures',0,'scanforlines',1,'sigtype','Channels','tau',100,'verb',1,'winsize',4,'winstep',1);
            EEG = eeg_checkset( EEG );

            % Filtering sampled data
            EEG = pop_eegfiltnew(EEG, 'locutoff',1,'plotfreqz',0);
            EEG = eeg_checkset( EEG );
            EEG = pop_eegfiltnew(EEG, 'hicutoff',15,'plotfreqz',0);
            EEG = eeg_checkset( EEG );
            
            
            %Saving
            EEG.setname= strcat(name, 'JS');
            EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Volumes/G-DRIVE mobile USB-C/EEG/jeans/SE');
            EEG = eeg_checkset( EEG );
            
        end
         
    end
    
end


%% ICA (AMICA)
% !careful!: Amica has problems with space in the name! 


b = struct('c', {'/Volumes/lasthope/EEG/eo', ...
    '/Volumes/lasthope/EEG/ec', ...
    '/Volumes/lasthope/EEG/energy/SE', ...
    '/Volumes/lasthope/EEG/energy/ELF', ...
    '/Volumes/lasthope/EEG/jeans/SE', ...
    '/Volumes/lasthope/EEG/jeans/ELF'});

l = struct('c',{'/Volumes/lasthope/EEG/postica/EO',...
    '/Volumes/lasthope/EEG/postica/EC', ...
    '/Volumes/lasthope/EEG/postica/EnSe',...
    '/Volumes/lasthope/EEG/postica/EnElf',...
    '/Volumes/lasthope/EEG/postica/JeSe',...
    '/Volumes/lasthope/EEG/postica/JeElf'});

% Variable if detecting bad channels

    %c = '/Volumes/G-DRIVE mobile USB-C/EEG/BC/'; 


% Variable if data needs to be interpolated afterwards (save data before detecting
% bad channels and later project ica weights, etc onto the original one

    %eegOriginal = struct('name', [], 'condition', [], 'chanlocs', []);
    

 % Define parameters for ICA (Amica method; default settings are use here)
    
    numprocs = 1;       % # of nodes (default = 1)
    max_threads = 2;    % # of threads per node
    num_models = 1;     % # of models of mixture ICA
    max_iter = 2000;    % max number of learning steps
  
    
    for i = 1:length(b)
        %disp("--------------->Loop<-------------")
        d = b(i).c;
        cd (d);
        
        eInfo = dir('*.set');
        eegData = {eInfo.name};
        
        for z = 1:length(eegData)
            
            a = eegData{z};
            name = a(1:9);
            
            EEG = pop_loadset('filename', a,'filepath', d);
            EEG = eeg_checkset( EEG );
            
            % Filtering Line Noise with eegplugin 'Cleanline'
            EEG = pop_cleanline(EEG, 'bandwidth',2,'chanlist',[1:32] ,'computepower',1,'linefreqs',50,'normSpectrum',0,'p',0.01,'pad',2,'plotfigures',0,'scanforlines',1,'sigtype','Channels','tau',100,'verb',1,'winsize',4,'winstep',1);
            EEG = eeg_checkset( EEG );
            
            % Keeping original EEG for interpolation (before bad channel
            % detection)
            
            %         eegOriginal(z).name = EEG.setname;
            %         eegOriginal(z).chanlocs = EEG.chanlocs;
            %         if i == 1
            %             eegOriginal(z).condition = 'EO';
            %         elseif i == 2
            %             eegOriginal(z).condition = 'EC';
            %         elseif i == 3
            %             eegOriginal(z).condition = 'ES';
            %         elseif i == 4
            %             eegOriginal(z).condition = 'EE';
            %         elseif i == 5
            %             eegOriginal(z).condition = 'JS';
            %         else
            %             eegOriginal(z).condition = 'JE';
            %         end
            
            
            %Bad channel detection (internet-Method; eeglab 2014 method)
            
            %[EEG, exChans] = pop_rejchan(EEG, 'elec',[1:EEG.nbchan],'threshold',2,'norm','on','measure','spec','freqrange',[0.3 40] );
            
            % Save information about Bad Channels excluded from ICA
            %(Adapted from Katrin Cunitz)
            
            %exChanFile = fullfile(c, [name(z), '_BCI.txt']); %BCI = Bad Channel Indicies
            %dlmwrite(exChanFile, exChans, 'delimiter', ',');
            %EEG = eeg_checkset( EEG );
            
            %leave out Fp1 & Fp2? --> eye blinks? leave it in --> ica will
            %detect eye blinks anyway; problem is that these channels are not
            %good to interpolate
            
            
            
            % Run ICA (AMICA Method)
            % type “help runamica15()” for a full list and explanation of the parameters
            
            if i == 1       % EO condition
                
                outdir = (l(i).c);
                [weights,sphere,mods] = runamica15(EEG.data, 'num_models',num_models, 'outdir',outdir, ...
                    'numprocs', numprocs, 'max_threads', max_threads, 'max_iter',max_iter);
                
                EEG.etc.amicaResultStructure = loadmodout15(outdir); %l(i).c
                EEG.icaweights = EEG.etc.amicaResultStructure.W;
                EEG.icasphere  = EEG.etc.amicaResultStructure.S;
                EEG = eeg_checkset( EEG );
                
                %Saving
                EEG.setname= strcat(name, '_icaed'); % removed line noise & used ICA
                EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath', l(i).c);
                EEG = eeg_checkset( EEG );
                
                
            elseif i == 2       % EC condition
                outdir = (l(i).c);
                [weights,sphere,mods] = runamica15(EEG.data, 'num_models',num_models, 'outdir',outdir, ...
                    'numprocs', numprocs, 'max_threads', max_threads, 'max_iter',max_iter);
                
                EEG.etc.amicaResultStructure = loadmodout15(l(i).c);
                EEG.icaweights = EEG.etc.amicaResultStructure.W;
                EEG.icasphere  = EEG.etc.amicaResultStructure.S;
                EEG = eeg_checkset( EEG );
                
                %Saving
                EEG.setname= strcat(name, '_icaed'); % removed line noise & used ICA
                EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath', l(i).c);
                EEG = eeg_checkset( EEG );
                
                
            elseif i == 3       % ES condition
                outdir = (l(i).c);
                [weights,sphere,mods] = runamica15(EEG.data, 'num_models',num_models, 'outdir',outdir, ...
                    'numprocs', numprocs, 'max_threads', max_threads, 'max_iter',max_iter);
                
                EEG.etc.amicaResultStructure = loadmodout15(l(i).c);
                EEG.icaweights = EEG.etc.amicaResultStructure.W;
                EEG.icasphere  = EEG.etc.amicaResultStructure.S;
                EEG = eeg_checkset( EEG );
                
                %Saving
                EEG.setname= strcat(name, '_icaed'); % removed line noise & used ICA
                EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath', l(i).c);
                EEG = eeg_checkset( EEG );
                
                
            elseif i == 4       % EE condition
                outdir = (l(i).c);
                [weights,sphere,mods] = runamica15(EEG.data, 'num_models',num_models, 'outdir',outdir, ...
                    'numprocs', numprocs, 'max_threads', max_threads, 'max_iter',max_iter);
                
                EEG.etc.amicaResultStructure = loadmodout15(l(i).c);
                EEG.icaweights = EEG.etc.amicaResultStructure.W;
                EEG.icasphere  = EEG.etc.amicaResultStructure.S;
                EEG = eeg_checkset( EEG );
                
                %Saving
                EEG.setname= strcat(name, '_icaed'); % removed line noise & used ICA
                EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath', l(i).c);
                EEG = eeg_checkset( EEG );
                
                
            elseif i == 5       %JS condition
                outdir = (l(i).c);
                [weights,sphere,mods] = runamica15(EEG.data, 'num_models',num_models, 'outdir',outdir, ...
                    'numprocs', numprocs, 'max_threads', max_threads, 'max_iter',max_iter);
                
                EEG.etc.amicaResultStructure = loadmodout15(l(i).c);
                EEG.icaweights = EEG.etc.amicaResultStructure.W;
                EEG.icasphere  = EEG.etc.amicaResultStructure.S;
                EEG = eeg_checkset( EEG );
                
                %Saving
                EEG.setname= strcat(name, '_icaed'); % removed line noise & used ICA
                EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath', l(i).c);
                EEG = eeg_checkset( EEG );
                
                
            else  % JE condition
                outdir = (l(i).c);
                [weights,sphere,mods] = runamica15(EEG.data, 'num_models',num_models, 'outdir',outdir, ...
                    'numprocs', numprocs, 'max_threads', max_threads, 'max_iter',max_iter);
                
                EEG.etc.amicaResultStructure = loadmodout15(l(i).c);
                EEG.icaweights = EEG.etc.amicaResultStructure.W;
                EEG.icasphere  = EEG.etc.amicaResultStructure.S;
                EEG = eeg_checkset( EEG );
                
                %Saving
                EEG.setname= strcat(name, '_icaed'); % removed line noise & used ICA
                EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath', l(i).c);
                EEG = eeg_checkset( EEG );
            end
            
            
        end
    end
    
%% Load files into EEGlab GUI to remove ocular artifacts (vertical & horizontal)

eeglab;

for i = 1:length(l)
    
    d = l(i).c;
    cd (d);
    
    eInfo = dir('*.set');
    eegData = {eInfo.name};
    
    
    for z = 1:length(eegData)
        EEG = pop_loadset('filename', eegData{z} ,'filepath', d);
        EEG = eeg_checkset( EEG );
        [ALLEEG, EEG, CURRENTSET] = eeg_store(ALLEEG, EEG);
        EEG = eeg_checkset(EEG);
        
        eeglab redraw;
    end
    
end

%% Testing area: amica

%attempt 1 [dolorme]
[EEG.icaweights, EEG.icawinv] = amica(EEG.data(:,:));
EEG.icasphere = eye(size(EEG.icaweights));
[ALLEEG, EEG, CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET);
eeglab redraw;

EEG = eeg_checkset( EEG );
EEG.setname= strcat(name, 'test');
EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/amicaout');

EEG.etc.amicaResultStructure = loadmodout15(['/data/projects/example/amicaResults/' dataName]);
EEG.icaweights = EEG.etc.amicaResultStructure.W;
EEG.icasphere  = EEG.etc.amicaResultStructure.S;

%% Testing Area: Amica WORKING!


cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/filt/Enfilt';
d = ['/Users/amycheung/Desktop/Masterarbeit/audiofiles/filt/Enfilt'];
eInfo = dir('*.set');
eegData = {eInfo.name};
        
for i = 1:length(eegData)
    

    EEG = pop_loadset('filename', eegData(i) ,'filepath', d);
    EEG = eeg_checkset( EEG );
    
    a = eegData{i};
    name = a(1:5);
    

    % define parameters
    numprocs = 1;       % # of nodes (default = 1)
    max_threads = 2;    % # of threads per node
    num_models = 1;     % # of models of mixture ICA
    max_iter = 2000;    % max number of learning steps

    % run amica
    outdir = ('/Users/amycheung/Desktop/Masterarbeit/audiofiles/amicaout');
    [weights,sphere,mods] = runamica15(EEG.data, 'num_models',num_models, 'outdir',outdir, ...
        'numprocs', numprocs, 'max_threads', max_threads, 'max_iter',max_iter);

    % type “help runamica15()” for a full list and explanation of the parameters

    EEG.etc.amicaResultStructure = loadmodout15(['/Users/amycheung/Desktop/Masterarbeit/audiofiles/amicaout']);
    EEG.icaweights = EEG.etc.amicaResultStructure.W;
    EEG.icasphere  = EEG.etc.amicaResultStructure.S;
    EEG = eeg_checkset( EEG );
    EEG.setname= strcat(name, 'work');
    EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/amicaout');
    

end
    
%% POST-ICA: Rereferencing (optional), Raw data inspection (Clean_raw)
clear; clc; 
%Variables for raw data inspection
EO = struct('name', [], 'latency', [], 'duration', []);
EC = struct('name', [], 'latency', [], 'duration', []);
EnSe = struct('name', [], 'latency', [], 'duration', []);
EnElf = struct('name', [], 'latency', [], 'duration', []);
JeSe = struct('name', [], 'latency', [], 'duration', []);
JeElf = struct('name', [], 'latency', [], 'duration', []);


l = struct('c',{'/Volumes/lasthope/EEG/removed_art/EO',...
    '/Volumes/lasthope/EEG/removed_art/EC', ...
    '/Volumes/lasthope/EEG/removed_art/energy/SE',...
    '/Volumes/lasthope/EEG/removed_art/energy/ELF',...
    '/Volumes/lasthope/EEG/removed_art/jeans/SE',...
    '/Volumes/lasthope/EEG/removed_art/jeans/ELF'});

%d = '/Users/amycheung/Desktop/Masterarbeit/audiofiles/clean/Energy';
%f = '/Users/amycheung/Desktop/Masterarbeit/audiofiles/fintest';

f = struct('c',{'/Volumes/lasthope/EEG/fin/EO',...
    '/Volumes/lasthope/EEG/fin/EC', ...
    '/Volumes/lasthope/EEG/fin/EnSe',...
    '/Volumes/lasthope/EEG/fin/EnElf',...
    '/Volumes/lasthope/EEG/fin/JeSe',...
    '/Volumes/lasthope/EEG/fin/JeElf'});

%folder = '/Users/amycheung/Desktop/Masterarbeit/audiofiles/rdi';


for i = 1:length(l)
    
    d = l(i).c;
    cd (d);
    
    eInfo = dir('*.set');
    eegData = {eInfo.name};
    
    for z = 1:length(eegData)

        a = eegData{z};
        name = a(1:9);
                
        EEG = pop_loadset('filename', a ,'filepath', d);
        EEG = eeg_checkset( EEG );
        
        % AVERAGE REFERENCING (take out because nose reference --> already contained in the signal itself)
        % 1) Performing average refernce after adding the original reference channel (i.e., zero-filled)
        % 2) Removing after average referencing, so the total number of
        % channels remains the same (Makoto Miyakoshi, 2017)
        
        % EEG = fullRankAveRef(EEG);
        
        % RAW DATA INSPECTION (ASR without removing bad channels)
        EEG = pop_clean_rawdata(EEG, 'FlatlineCriterion','off','ChannelCriterion','off','LineNoiseCriterion','off','Highpass','off','BurstCriterion',20,'WindowCriterion',0.25,'BurstRejection','on','Distance','Euclidian','WindowCriterionTolerances',[-Inf 7] );
        EEG = eeg_checkset( EEG );
        
        if i == 1 %EO
            clear EO;
            EO = struct('name', [], 'latency', [], 'duration', []);
            for u = 1:numel(EEG.event) 
                if (ismember({EEG.event(u).type}, {'boundary'})) == 1
                    EO(u).name = name;
                    EO(u).latency = EEG.event(u).latency;
                    EO(u).duration = EEG.event(u).duration;
                    
                else
                    disp('f this');
                end
            end

            rdi = strcat('rdi_', name, '.mat');
            folder = '/Volumes/lasthope/EEG/rdi/EO';
            fullFile = fullfile(folder, rdi);
            save(fullFile, 'EO');
            
        elseif i == 2 %EC
            EC = struct('name', [], 'latency', [], 'duration', []);
            for u = 1:numel(EEG.event)
                if (ismember({EEG.event(u).type}, {'boundary'})) == 1
                    EC(u).name = name;
                    EC(u).latency = EEG.event(u).latency;
                    EC(u).duration = EEG.event(u).duration;
                    
                else
                    disp('f this');
                end
            end
            rdi = strcat('rdi_', name, '.mat');
            folder = '/Volumes/lasthope/EEG/rdi/EC';
            fullFile = fullfile(folder, rdi);
            save(fullFile, 'EC')
            
        elseif i == 3 %EnSe
            EnSe = struct('name', [], 'latency', [], 'duration', []);
            for u = 1:numel(EEG.event)
                if (ismember({EEG.event(u).type}, {'boundary'})) == 1
                    EnSe(u).name = name;
                    EnSe(u).latency = EEG.event(u).latency;
                    EnSe(u).duration = EEG.event(u).duration;
                else
                    disp('f this');
                end
            end
            rdi = strcat('rdi_', name, '.mat');
            folder = '/Volumes/lasthope/EEG/rdi/EnSe';
            fullFile = fullfile(folder, rdi);
            save(fullFile, 'EnSe')
            
        elseif i == 4 %EnElf
            EnElf = struct('name', [], 'latency', [], 'duration', []);
            for u = 1:numel(EEG.event)
                if (ismember({EEG.event(u).type}, {'boundary'})) == 1
                    EnElf(u).name = name;
                    EnElf(u).latency = EEG.event(u).latency;
                    EnElf(u).duration = EEG.event(u).duration;
                    
                else
                    disp('f this');
                end
            end
            rdi = strcat('rdi_', name, '.mat');
            folder = '/Volumes/lasthope/EEG/rdi/EnElf';
            fullFile = fullfile(folder, rdi);
            save(fullFile, 'EnElf')
            
        elseif i == 5 %JeSe
            JeSe = struct('name', [], 'latency', [], 'duration', []);
            for u = 1:numel(EEG.event)
                if (ismember({EEG.event(u).type}, {'boundary'})) == 1
                    JeSe(u).name = name;
                    JeSe(u).latency = EEG.event(u).latency;
                    JeSe(u).duration = EEG.event(u).duration;
                else
                    disp('f this');
                end
            end
            rdi = strcat('rdi_', name, '.mat');
            folder = '/Volumes/lasthope/EEG/rdi/JeSe';
            fullFile = fullfile(folder, rdi);
            save(fullFile, 'JeSe')
            
        else %JeElf
            JeElf = struct('name', [], 'latency', [], 'duration', []);
            for u = 1:numel(EEG.event)
                if (ismember({EEG.event(u).type}, {'boundary'})) == 1
                    JeElf(u).name = name;
                    JeElf(u).latency = EEG.event(u).latency;
                    JeElf(u).duration = EEG.event(u).duration;
                else
                    disp('f this');
                end
            end
            rdi = strcat('rdi_', name, '.mat');
            folder = '/Volumes/lasthope/EEG/rdi/JeElf';
            fullFile = fullfile(folder, rdi);
            save(fullFile, 'JeElf')
        end
        
        %ica weights should be transfered as well!
        %EEG = pop_interp(EEG, originalEEG.chanlocs, 'spherical');
        
        
        %Saving
        
        EEG.setname= strcat(name, '_fin'); 
        EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath', f(i).c);
        EEG = eeg_checkset( EEG );
        
    end
end

%% Remove equivalent parts from the raw data inspection from the audio signal


l = struct('c',{'/Volumes/lasthope/EEG/rdi/EO/Pro',...
    '/Volumes/lasthope/EEG/rdi/EO/Stu',...
    '/Volumes/lasthope/EEG/rdi/EO/mPro',...
    '/Volumes/lasthope/EEG/rdi/EO/mStu',...
    '/Volumes/lasthope/EEG/rdi/EC/Pro',...
    '/Volumes/lasthope/EEG/rdi/EC/Stu',...
    '/Volumes/lasthope/EEG/rdi/EC/mPro',...
    '/Volumes/lasthope/EEG/rdi/EC/mStu',...
    '/Volumes/lasthope/EEG/rdi/EnSe/Pro',...
    '/Volumes/lasthope/EEG/rdi/EnSe/Stu',...
    '/Volumes/lasthope/EEG/rdi/EnSe/mPro',...
    '/Volumes/lasthope/EEG/rdi/EnSe/mStu',...
    '/Volumes/lasthope/EEG/rdi/EnElf/Pro',...
    '/Volumes/lasthope/EEG/rdi/EnElf/Stu',...
    '/Volumes/lasthope/EEG/rdi/EnElf/mPro',...
    '/Volumes/lasthope/EEG/rdi/EnElf/mStu',...
    '/Volumes/lasthope/EEG/rdi/JeSe/Pro',...
    '/Volumes/lasthope/EEG/rdi/JeSe/Stu',...
    '/Volumes/lasthope/EEG/rdi/JeSe/mPro',...
    '/Volumes/lasthope/EEG/rdi/JeSe/mStu',...
    '/Volumes/lasthope/EEG/rdi/JeElf/Pro',...
    '/Volumes/lasthope/EEG/rdi/JeElf/Stu',...
    '/Volumes/lasthope/EEG/rdi/JeElf/mPro',...
    '/Volumes/lasthope/EEG/rdi/JeElf/mStu'});

%d = '/Volumes/lasthope/EEG/rdi/EnElf/';

EnseAudioPro = struct('name', [], 'data', []);


cd '/Volumes/lasthope/EEG/audiofiles';
audInfo = dir('*.wav');
audio = {audInfo.name};

for i = 9:12 %length(l)
    
  if i == 9 || i == 10 || i == 11 || i == 12 %EnSe

      [y, fs] = audioread(audInfo(2).name); %y = audio signal; fs= sampling rate
      info = audioinfo(audInfo(2).name);
      timeArray = 0:seconds(1/fs):seconds(info.Duration);
      timeArray = timeArray(1:end-1);
      signalArray = y(:,1); %using signal data of the left ear (channel 1)
      
      audioSamp = resample(signalArray, 250, 48000); %downsample audio data
      audioSamprem = audioSamp;
      
  elseif i == 13 || i == 14 || i == 15 || i == 16 %EnElf
      [y, fs] = audioread(audInfo(1).name); %y = audio signal; fs= sampling rate
      info = audioinfo(audInfo(1).name);
      timeArray = 0:seconds(1/fs):seconds(info.Duration);
      timeArray = timeArray(1:end-1);
      signalArray = y(:,1); %using signal data of the left ear (channel 1)
      
      audioSamp = resample(signalArray, 250, 48000); %downsample audio data
      audioSamprem = audioSamp;
      
  elseif i == 17 || i == 18 || i == 19 || i == 20 %JeSe
      [y, fs] = audioread(audInfo(4).name); %y = audio signal; fs= sampling rate
      info = audioinfo(audInfo(4).name);
      timeArray = 0:seconds(1/fs):seconds(info.Duration);
      timeArray = timeArray(1:end-1);
      signalArray = y(:,1); %using signal data of the left ear (channel 1)
      
      audioSamp = resample(signalArray, 250, 48000); %downsample audio data
      audioSamprem = audioSamp;
      
  else %JeElf 21
      [y, fs] = audioread(audInfo(3).name); %y = audio signal; fs= sampling rate
      info = audioinfo(audInfo(3).name);
      timeArray = 0:seconds(1/fs):seconds(info.Duration);
      timeArray = timeArray(1:end-1);
      signalArray = y(:,1); %using signal data of the left ear (channel 1)
      
      audioSamp = resample(signalArray, 250, 48000); %downsample audio data
      audioSamprem = audioSamp;
      
  end
  
  
  cd(l(12).c);
  
  
  rdi = dir('*.mat');
  rdi = {rdi.name};
  
    %if i == 9 || i == 10 || i == 11 || i == 12 %EnSe
        
        
        
        
        for z = 1:length(rdi)
            
            audioSamprem = audioSamp;
            a = rdi{z};
            name = a(5:13);
            
            load(a)
            
        
            for o = 1:numel(EnSe)
                
                if ~isnan(EnSe(o).latency) & (str2double(sprintf('%.f', EnSe(o).latency)))-30 > 0
                    j1 = (str2double(sprintf('%.f', EnSe(o).latency)))-30; %time lags between 110-130ms resulted in best mapping -> 120ms were used (30pnts)
                    j2 = (j1 + (EnSe(o).duration));
                    %j2 = str2double(sprintf('%.f', j2));
                    
                    audioSamprem([j1:j2], 1) = NaN;
                    
                elseif ~isnan(EnSe(o).latency) & (str2double(sprintf('%.f', EnSe(o).latency)))-30 <= 0
                    j1 = (str2double(sprintf('%.f', EnSe(o).latency)))+1; %time lags between 110-130ms resulted in best mapping -> 120ms were used (30pnts)
                    j2 = (j1 + (EnSe(o).duration));
                    %j2 = str2double(sprintf('%.f', j2));
                    
                    audioSamprem([j1:j2], 1) = NaN;
                    
                elseif isempty(EnSe(o).latency) == 1
                    disp('f this');
                    %JeElf(o) = [];
       
                end
            end
            audioSamprem = audioSamprem(~isnan(audioSamprem));
            EnSeAudiomStu(z).name = name;
            EnSeAudiomStu(z).data = audioSamprem;
        end
        
        %JEaudio = 'JeElfAudioPro.mat';
        folder = '/Volumes/lasthope/EEG/audiofiles';
        fullFile = fullfile(folder, 'EnSeAudiomStu.mat');
        save(fullFile, 'EnSeAudiomStu');
        
        
        
    elseif i == 13 || i == 14 || i == 15 || i == 16 %EnElf
        
        for z = 1:length(rdi)
            
            a = rdi{z};
            name = a(5:13);
            
            
            load(a)
            
            for o = 1:numel(EO)
                if ~isnan(EO(o).latency)
                    j1 = (str2double(sprintf('%.f', EO(o).latency)));%-30; %time lags between 110-130ms resulted in best mapping -> 120ms were used (30pnts)
                    j2 = (j1 + (EO(o).duration));
                    %j2 = str2double(sprintf('%.f', j2));
                    
                    audioSamprem([j1:j2], 1) = NaN;
                else
                    disp('f this');
                    
                end
            end
            audioSamprem = audioSamprem(~isnan(audioSamprem));
        end
    end
end




EEG = pop_loadset('filename', 'DBA_RZBEE_fin.set' ,'filepath', '/Volumes/lasthope/EEG/fin/EnElf');
EEG = eeg_checkset( EEG );

%% Interpolating (after ICA)

q = struct('c',{'/Users/amycheung/Desktop/Masterarbeit/audiofiles/fin/EO',...
    '/Users/amycheung/Desktop/Masterarbeit/audiofiles/fin/EC', ...
    '/Users/amycheung/Desktop/Masterarbeit/audiofiles/fin/EnSe',...
    '/Users/amycheung/Desktop/Masterarbeit/audiofiles/fin/EnElf',...
    '/Users/amycheung/Desktop/Masterarbeit/audiofiles/fin/JeSe',...
    '/Users/amycheung/Desktop/Masterarbeit/audiofiles/fin/JeElf'});


for i = 1:length(l)
    
    d = l(i).c;
    cd (d);
    
    f = q(i).c;
    
    eInfo = dir('*.set');
    eegData = {eInfo.name};
    
    for z = 1:length(eegData)
        
        a = eegData{z};
        name = a(1:7);
        
        EEG = pop_loadset('filename', eegData(z),'filepath', d');
        EEG = eeg_checkset( EEG );
        
        EEG = pop_interp(EEG, originalEEG.chanlocs, 'spherical');
        
        
        %Saving
        EEG.setname= strcat(name, '_fin'); %
        EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath', f);
        EEG = eeg_checkset( EEG );
    
    end

end




%% Break new/old
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        BREAK
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Loading imported eeg data and continue epoching (EC)

for i = 1:length(eegData)
    
    a = eegData{i};
    name = a(3:5);
    %name = a(5:7) -> for the real data: example "MPR_DTH" (take the 5:7)
        
    EEG = pop_loadbv('/Users/amycheung/Desktop/Masterarbeit/audiofiles', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
    EEG.setname = strcat(name, 'EEG'); %saved the imported data
    EEG = eeg_checkset( EEG );
    
    %EEG = pop_loadset('filename', a ,'filepath', '/Users/amycheung/Desktop/Masterarbeit/audiofiles');
    %EEG = eeg_checkset( EEG );

    %extracting epoch EC
    %EO marker = 'S 81'
    %trial duration ~ 189sec (adding additional 2sec for buffer when
    %filtering data)
    EEG = pop_rmdat( EEG, {'S 81'},[0 189] ,0);
    EEG.setname= strcat(name, 'EC');
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
    EEG = pop_editeventvals(EEG,'delete',1);%delete second 'boundary' marker
    EEG = eeg_checkset( EEG );
    %setting channel locations
    EEG=pop_chanedit(EEG, 'lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp','lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp');
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC/');
    EEG = eeg_checkset( EEG );
    
end

%% Loading imported eeg data and continue epoching (Energy)

cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/EnergySE';

eInfo = dir('*.vhdr'); %just take the .vhdr-files
eegData = {eInfo.name}; %files are listed in each column

for i = 1:length(eegData)
    
    a = eegData{i};
    name = a(3:5);
    %name = a(5:7) -> for the real data: example "MPR_DTH" (take the 5:7)
    
    %loading original eeg data again
    
    EEG = pop_loadbv('/Users/amycheung/Desktop/Masterarbeit/audiofiles', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
    EEG.setname = strcat(name, 'EEG'); %saved the imported data
    EEG = eeg_checkset( EEG );
    
    %EEG = pop_loadset('filename', o,'filepath','/Users/amycheung/Documents/Masterarbeit/audiofiles/EEG/');
    %EEG = eeg_checkset( EEG );
    
    %extracting epoch Energy
    %EO marker = 'S 22'
    %trial duration ~ 373sec (adding additional 2sec for buffer when
    %filtering data)
    EEG = pop_rmdat( EEG, {'S 22'},[0 374] ,0);
    EEG.setname= strcat(name, 'Energy');
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1); %delete second 'boundary' marker
    EEG = eeg_checkset( EEG );
    %setting channel locations
    EEG=pop_chanedit(EEG, 'lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp','lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp');
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy/');
    EEG = eeg_checkset( EEG ); %end file should contain 2 markers ('S 22' and 'S 52')
    
end


%% Loading imported eeg data and continue epoching (Jeans)

for i = 1:length(eegData)
    
    a = eegData{i};
    name = a(3:5);
    %name = a(5:7) -> for the real data: example "MPR_DTH" (take the 5:7)
    
    %loading orginal eeg data again
    
    EEG = pop_loadbv('/Users/amycheung/Desktop/Masterarbeit/audiofiles', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
    EEG.setname = strcat(name, 'EEG'); %saved the imported data
    EEG = eeg_checkset( EEG );
    
    %EEG = pop_loadset('filename', o,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles');
    %EEG = eeg_checkset( EEG );
    
    %extracting epoch Jeans
    %EO marker = 'S 23'
    %trial duration ~ 413.8sec (adding additional 2sec for buffer when
    %filtering data)
    EEG = pop_rmdat( EEG, {'S 23'},[0 414] ,0);
    EEG.setname= strcat(name, 'Jeans');
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1);%delete second 'boundary' marker
    EEG = eeg_checkset( EEG );
    %setting channel locations
    EEG=pop_chanedit(EEG, 'lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp','lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp');
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/');
    EEG = eeg_checkset( EEG );
    
end

%Script description:
%importing original data file and extracting trial epochs

%% Referencing & Downsampling
%used average reference:
%Average Energy on the scalp (sum all the electrodes) should be 0

%Downsampling the epochs to 250Hz
%-> Matlab will automatically lowpass filter the dataset to the half of the
%sampling rate. For example 250Hz -> 125Hz lowpass filter
%250Hz = 1 data point per 4ms
%--> sampling rate can't be too low because of aliasing (Nyquist theory: at
%least 2times higher than frequency of interest)
%depending on research goal a higher sampling rate is more fitted, for
%example high oscillatory dynamics (30-100Hz) -> 1000Hz/2000Hz more
%sufficient [https://wiki.cimec.unitn.it/tiki-index.php?page=Data+pre-processing]

%Re-referencing: the data was probably online-referenced -> that's why no
%rereferencing in eeglab and also there is no such location-file of
%brain-vision


%[EEG, ALLEEG, CURRENTSET, LASTCOM, ALLCOM] = eeglab; doesn't need to
%initialize again


b = struct('c', {'/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/', '/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy/','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC', '/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO/'});

for i = 1:length(b)
    
    d = b(i).c
    cd (d);
    
    eInfo = dir('*.set'); %just take the .set-files
    Epoch = {eInfo.name};
    
    for j = 1:length(Epoch)
        
        a = Epoch{j};
        name = a(1:5);
        
        EEG = pop_loadset('filename', a,'filepath', d);
        %EEG = eeg_checkset( EEG );
        %EEG = pop_reref( EEG, []); %rereferencing with averaging methode
        EEG = eeg_checkset( EEG );
        EEG = pop_resample( EEG, 250); %resampling datasets from 5000Hz to 250Hz (eeglab automatically lowpass filters new dataset to half the sampling rate)
        EEG.setname= strcat(name, 'r'); %rr = 'rereferenced' & 'resampled'
        EEG = eeg_checkset( EEG );
        
        
        %Filtering sampled data
        EEG = pop_eegfiltnew(EEG, 'hicutoff',15,'plotfreqz',0);
        EEG = eeg_checkset( EEG );
        EEG = pop_eegfiltnew(EEG, 'locutoff',1,'plotfreqz',0);
        EEG = eeg_checkset( EEG );
        
        %notch filter (50Hz) Line Noise
        %test: EEG = pop_eegfiltnew(EEG, 'locutoff',14.5,'hicutoff',15.5,'revfilt',1,'plotfreqz',0);
        %EEG = pop_eegfiltnew(EEG, 'locutoff',49.5,'hicutoff',50.5,'revfilt',1,'plotfreqz',0);
        %EEG = eeg_checkset( EEG );
        
        %low-pass filtering
        %     EEG = pop_eegfiltnew(EEG, 'hicutoff',30, 'plotfreqz',1);
        %     EEG.setname= strcat(name, '_filt');
        %     EEG = eeg_checkset( EEG );
        
        EEG.setname= strcat(name, '_filt');
        EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/filt');
        EEG = eeg_checkset( EEG );

    end
end


cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC';

eInfo = dir('*.set'); %just take the .set-files
ECepoch = {eInfo.name};

%[EEG, ALLEEG, CURRENTSET, LASTCOM, ALLCOM] = eeglab;
%Maybe have to initialize eeglab due to warning (but still works without
%though)

for i = 1:length(ECepoch)
    
    a = ECepoch{i};
    name = a(1:5);
    
    EEG = pop_loadset('filename', a,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC/');
    %EEG = eeg_checkset( EEG );
    %EEG = pop_reref( EEG, []); %rereferencing with averaging methode
    EEG = eeg_checkset( EEG );
    EEG = pop_resample( EEG, 250); %resampling datasets from 5000Hz to 250Hz (eeglab automatically lowpass filters new dataset to half the sampling rate)
    EEG.setname= strcat(name, 'r'); %rr = 'rereferenced' & 'resampled' 
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC/');
    EEG = eeg_checkset( EEG );
    
end


%Doing the same with EO-epochs

cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO';

eInfo = dir('*.set'); %just take the .set-files
EOepoch = {eInfo.name};


for i = 1:length(EOepoch)
    
    a = EOepoch{i};
    name = a(1:5);
    
    EEG = pop_loadset('filename', a,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO/');
    %EEG = eeg_checkset( EEG );
    %EEG = pop_reref( EEG, []); %rereferencing with averaging methode
    EEG = eeg_checkset( EEG );
    EEG = pop_resample( EEG, 250); %resampling datasets from 5000Hz to 250Hz (eeglab automatically lowpass filters new dataset to half the sampling rate)
    EEG.setname= strcat(name, 'r'); %rr = 'rereferenced' & 'resampled' 
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO/samp');
    EEG = eeg_checkset( EEG );
    
end


%Doing the same with Energy-epochs

cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy';

eInfo = dir('*.set'); %just take the .set-files
Eepoch = {eInfo.name};

for i = 1:length(Eepoch)
    
    a = Eepoch{i};
    name = a(1:5);
    
    EEG = pop_loadset('filename', a,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy/');
    %EEG = eeg_checkset( EEG );
    %EEG = pop_reref( EEG, []); %rereferencing with averaging methode
    EEG = eeg_checkset( EEG );
    EEG = pop_resample( EEG, 250); %resampling datasets from 5000Hz to 250Hz (eeglab automatically lowpass filters new dataset to half the sampling rate)
    EEG.setname= strcat(name, 'r'); %rr = 'rereferenced' & 'resampled' 
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy/');
    EEG = eeg_checkset( EEG );
    
end


%Doing the same with Jeans-epochs

cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans';

eInfo = dir('*.set'); %just take the .set-files
Jepoch = {eInfo.name};

for i = 1:length(Jepoch)
    
    a = Jepoch{i};
    name = a(1:5);
    
    EEG = pop_loadset('filename', a,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/');
    EEG = eeg_checkset( EEG );
    %EEG = pop_reref( EEG, []); %rereferencing with averaging methode
    %EEG = eeg_checkset( EEG );
    EEG = pop_resample( EEG, 250); %resampling datasets from 5000Hz to 250Hz (eeglab automatically lowpass filters new dataset to half the sampling rate)
    EEG.setname= strcat(name, 'r'); %rr = 'rereferenced' & 'resampled' 
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/');
    EEG = eeg_checkset( EEG );
    
end


%Section description: rereferencing data and downsample epochs to 250Hz

%% Filter Epochs
%till now epochs have been lowpass-filterd to 125Hz
%DC offset is already removed
%there is no phase-shift in eeglab (filtering forward and backward -> cancelles phase-shift)

%suggestion by stackoverflow
% [b, a] = butter(order, cutoff/Fs, 'high');
% data = filter(b, a, data);

%to remove line noise (50Hz) they recommend to use the eeglab plugin
%"Cleanline" -> also take a look on this paper:
%Widmann, Schröger, Maess, 2015. Digital filter design for
%electrophysiological data - a practical approach. J Neurosci Method.
%A short review and explanation of Cleanline: https://www.nitrc.org/projects/cleanline/

% EEG = pop_eegfilt( EEG, 1, 0); %filtering eeg data without popping GUI


%%% Filtering EO data sets %%% --> 

cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO/samp';

eInfo = dir('*.set'); %just take the .set-files
EOsamp = {eInfo.name};


for i = 1:length(EOsamp)
    
    a = EOsamp{i};
    name = a(1:5);
    
    %high-pass filtering
    EEG = pop_loadset('filename', a,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO/samp/');
    EEG = pop_eegfiltnew(EEG, 'locutoff',0.1,'plotfreqz',0);
    EEG = eeg_checkset( EEG );
    
    %notch filter (50Hz) Line Noise
    EEG = pop_eegfiltnew(EEG, 'locutoff',49.5,'hicutoff',50.5,'revfilt',1,'plotfreqz',0);
    EEG = eeg_checkset( EEG );
    
    %low-pass filtering
    %     EEG = pop_eegfiltnew(EEG, 'hicutoff',30, 'plotfreqz',1);
    %     EEG.setname= strcat(name, '_filt');
    %     EEG = eeg_checkset( EEG );
    
    EEG.setname= strcat(name, '_filt');
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO/filt');
    EEG = eeg_checkset( EEG );
    
end



%%% Filtering EC data sets %%% --> filtering good

cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC/samp';

eInfo = dir('*.set'); %take the resampled EC.set files in the samp folder
ECsamp = {eInfo.name};

for i = 1:length(ECsamp)
    
    a = ECsamp{i};
    name = a(1:5);
    
    %high-pass filtering
    EEG = pop_loadset('filename', a,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC/samp/');
    EEG = pop_eegfiltnew(EEG, 'locutoff',0.1,'plotfreqz',0);
    EEG = eeg_checkset( EEG );
    
    %notch filter (50Hz) Line Noise
    EEG = pop_eegfiltnew(EEG, 'locutoff',49.5,'hicutoff',50.5,'revfilt',1,'plotfreqz',0);
    EEG = eeg_checkset( EEG );
    %best filter frequencies: 49.45-50.55Hz
    
    %low-pass filtering
    %     EEG = pop_eegfiltnew(EEG, 'hicutoff',30, 'plotfreqz',0);
    %     EEG.setname= strcat(name, '_filt');
    %     EEG = eeg_checkset( EEG );
    
    EEG.setname= strcat(name, '_filt');
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC/filt');
    EEG = eeg_checkset( EEG );
    
end



%%% Filtering Energy data sets %%% --> filtering good

cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy/samp';

eInfo = dir('*.set'); %take the resampled Energy.set files in the samp folder
Ensamp = {eInfo.name};

for i = 1:length(Ensamp)
    
    a = Ensamp{i};
    name = a(1:5);
    
    %high-pass filtering
    EEG = pop_loadset('filename', a,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy/samp/');
    EEG = pop_eegfiltnew(EEG, 'locutoff',0.1,'plotfreqz',0);
    EEG = eeg_checkset( EEG );
    
    %notch filter (50Hz) Line Noise
    EEG = pop_eegfiltnew(EEG, 'locutoff',49.5,'hicutoff',50.5,'revfilt',1,'plotfreqz',0);
    EEG = eeg_checkset( EEG );
    %best filter frequencies: 49.7-50.3
    
    %low-pass filtering
    %     EEG = pop_eegfiltnew(EEG, 'hicutoff',30, 'plotfreqz',0);
    %     EEG.setname= strcat(name, '_filt');
    %     EEG = eeg_checkset( EEG );
    
    EEG.setname= strcat(name, '_filt');
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy/filt');
    EEG = eeg_checkset( EEG );
    
end


%%% Filtering Jeans data sets %%% --> filtering good

cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/samp';

eInfo = dir('*.set'); %take the resampled Energy.set files in the samp folder
Jsamp = {eInfo.name};

for i = 1:length(Jsamp)
    
    a = Jsamp{i};
    name = a(1:5);
    
    %high-pass filtering
    EEG = pop_loadset('filename', a,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/samp/');
    EEG = pop_eegfiltnew(EEG, 'locutoff',0.1,'plotfreqz',0);
    EEG = eeg_checkset( EEG );
    
    %notch filter (50Hz) Line Noise
    EEG = pop_eegfiltnew(EEG, 'locutoff',49.5,'hicutoff',50.5,'revfilt',1,'plotfreqz',0);
    EEG = eeg_checkset( EEG );
    
    %low-pass filtering
    %     EEG = pop_eegfiltnew(EEG, 'hicutoff',30, 'plotfreqz',0);
    %     EEG.setname= strcat(name, '_filt');
    %     EEG = eeg_checkset( EEG );
    
    EEG.setname= strcat(name, '_filt');
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/filt');
    EEG = eeg_checkset( EEG );
    
end


b = struct('c', {'/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/filt/', '/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy/filt/','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC/filt/', '/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO/filt/'});

for i = 1:length(b)
    
    d = b(i).c
    cd (d);
    
    icaInfo = dir('*.set'); %take filtered files from filt folder
    icaDat = {icaInfo.name};

    for j = 1:length(icaDat)
        
        a = icaDat{j};
        name = a(1:5);
        
    EEG = pop_loadset('filename', a,'filepath', d);
    EEG = eeg_checkset( EEG );
    EEG = pop_runica(EEG, 'icatype', 'runica', 'extended',1,'interrupt','on');
    EEG = eeg_checkset( EEG );
    
    EEG.setname= strcat(name, '_ica');
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/ICA');
    EEG = eeg_checkset( EEG );

    end
end


%todo: test if it's better to do a low-,high- and notch- filter or
%low-, high- and notch



%% ICA

%b = struct('c', {'/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/filt/', '/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy/filt/','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC/filt/', '/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO/filt/'});
b = '/Users/amycheung/Desktop/Masterarbeit/audiofiles/filt';
for i = 1:length(b)
    
    d = b(i).c
    cd (d);
    
    icaInfo = dir('*.set'); %take filtered files from filt folder
    icaDat = {icaInfo.name};

    for j = 1:length(icaDat)
        
        a = icaDat{j};
        name = a(1:5);
        
    EEG = pop_loadset('filename', a,'filepath', d);
    EEG = eeg_checkset( EEG );
    EEG = pop_runica(EEG, 'icatype', 'runica', 'extended',1,'interrupt','on');
    EEG = eeg_checkset( EEG );
    
    EEG.setname= strcat(name, '_ica');
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/ICA');
    EEG = eeg_checkset( EEG );

    end
end
