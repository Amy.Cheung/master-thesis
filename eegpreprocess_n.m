%% Preprocessing EEG-data

clear, clc;

%cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles'; %working directory on mac
cd 'C:\Users\Amy Man Ji Cheung\Desktop\master-thesis\data' %working directory on windows

eInfo = dir('*.vhdr'); %just take the .vhdr-files
eegData = {eInfo.name}; %files are listed in each column

%[X] mkdir 'EEG'; Löschen: importierte EEG Daten werden nicht mehr gespeichert
    

[EEG, ALLEEG, CURRENTSET, LASTCOM, ALLCOM] = eeglab; %to initialize eeglab
%maybe do not even have to initialize eeglab to import and process data

for i = 1:length(eegData) %index for all the files
    
    a = eegData{i};
    name = a(3:5);
    %name = a(5:7) -> for the real data: example "MPR_DTH" (take the 5:7)
    
    %importing original raw eeg data
    %for mac:
    %EEG = pop_loadbv('/Users/amycheung/Desktop/Masterarbeit/audiofiles', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]); %choosing 32 channels because don't need the last two; 33 = EKG; 34 = GSR
    %for windows:
    EEG = pop_loadbv('C:\Users\Amy Man Ji Cheung\Desktop\master-thesis\data', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]); %choosing 32 channels because don't need the last two; 33 = EKG; 34 = GSR
    EEG.setname = strcat(name, 'EEG'); %saved the imported data
    EEG = eeg_checkset( EEG );
    
    %extracting epoch EO
    %EO marker = 'S 83'
    %trial duration ~ 189sec (adding additional 2sec for buffer when
    %filtering data)
    EEG = pop_rmdat( EEG, {'S 83'},[-2 191] ,0);
    EEG.setname= strcat(name, 'EO');
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1);%delete second 'boundary' marker
    EEG = eeg_checkset( EEG );
    %for mac:
    %EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO/');
    %for windows:
    EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','C:\Users\Amy Man Ji Cheung\Desktop\master-thesis\data\EO\');
    EEG = eeg_checkset( EEG );
    
    [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG);
    eeglab redraw;
    
end

%% Loading imported eeg data and continue epoching (EC)

for i = 1:length(eegData)
    
    a = eegData{i};
    name = a(3:5);
    %name = a(5:7) -> for the real data: example "MPR_DTH" (take the 5:7)
    
    
    %loading original eeg data again
    
    %for mac:
    %EEG = pop_loadbv('/Users/amycheung/Desktop/Masterarbeit/audiofiles', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
    %for windows:
    EEG = pop_loadbv('C:\Users\Amy Man Ji Cheung\Desktop\master-thesis\data', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
    EEG.setname = strcat(name, 'EEG'); %saved the imported data
    EEG = eeg_checkset( EEG );
    

    %extracting epoch EC
    %EO marker = 'S 81'
    %trial duration ~ 189sec (adding additional 2sec before and after for buffer when filtering data)
    EEG = pop_rmdat( EEG, {'S 81'},[-2 191] ,0);
    EEG.setname= strcat(name, 'EC');
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1);%delete second 'boundary' marker
    EEG = eeg_checkset( EEG );
    %for mac:
    %EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC/');
    %for windows:
    EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','C:\Users\Amy Man Ji Cheung\Desktop\master-thesis\data\EC\');
    EEG = eeg_checkset( EEG );
    
    [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG);
    eeglab redraw;
    
end

%% Loading imported eeg data and continue epoching (Energy)

for i = 1:length(eegData)
    
    a = eegData{i};
    name = a(3:5);
    %name = a(5:7) -> for the real data: example "MPR_DTH" (take the 5:7)
    
    
    %loading original eeg data again
    
    %for mac:
    %EEG = pop_loadbv('/Users/amycheung/Desktop/Masterarbeit/audiofiles', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
    %for windows:
    EEG = pop_loadbv('C:\Users\Amy Man Ji Cheung\Desktop\master-thesis\data', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
    EEG.setname = strcat(name, 'EEG'); %saved the imported data
    EEG = eeg_checkset( EEG );
    
    
    %extracting epoch Energy
    %EO marker = 'S 22'
    %trial duration ~ 373sec (adding additional 2sec for buffer when
    %filtering data)
    EEG = pop_rmdat( EEG, {'S 22'},[-2 373.65] ,0);
    EEG.setname= strcat(name, 'Energy');
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1); %delete second 'boundary' marker
    EEG = eeg_checkset( EEG );
    %for mac:
    %EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy/');
    %for windows:
    EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','C:\Users\Amy Man Ji Cheung\Desktop\master-thesis\data\Energy');
    EEG = eeg_checkset( EEG ); %end file should contain 2 markers ('S 22' and 'S 52')
    
    [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG);
    eeglab redraw;
end


%% Loading imported eeg data and continue epoching (Jeans)

for i = 1:length(eegData)
    
    a = eegData{i};
    name = a(3:5);
    %name = a(5:7) -> for the real data: example "MPR_DTH" (take the 5:7)
    
    
    %loading orginal eeg data again
    
    %for mac:
    %EEG = pop_loadbv('/Users/amycheung/Desktop/Masterarbeit/audiofiles', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
    %for windows:
    EEG = pop_loadbv('C:\Users\Amy Man Ji Cheung\Desktop\master-thesis\data', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
    EEG.setname = strcat(name, 'EEG'); %saved the imported data
    EEG = eeg_checkset( EEG );
    
    
    %extracting epoch Jeans
    %EO marker = 'S 23'
    %trial duration ~ 413.8sec (adding additional 2sec for buffer when
    %filtering data)
    EEG = pop_rmdat( EEG, {'S 23'},[-2 415] ,0);
    EEG.setname= strcat(name, 'Jeans');
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1);%delete second 'boundary' marker
    EEG = eeg_checkset( EEG );
    %for mac:
    %EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/');
    %for windows:
    EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','C:\Users\Amy Man Ji Cheung\Desktop\master-thesis\data\Jeans\');
    EEG = eeg_checkset( EEG );
    
    [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG);
    eeglab redraw;
end

%Script description:
%importing original data file and extracting trial epochs

%% Referencing & Downsampling
%used average reference:
%Average Energy on the scalp (sum all the electrodes) should be 0

%Downsampling the epochs to 250Hz
%-> Matlab will automatically lowpass filter the dataset to the half of the
%sampling rate. For example 250Hz -> 125Hz lowpass filter
%250Hz = 1 data point per 4ms
%--> sampling rate can't be too low because of aliasing (Nyquist theory: at
%least 2times higher than frequency of interest)
%depending on research goal a higher sampling rate is more fitted, for
%example high oscillatory dynamics (30-100Hz) -> 1000Hz/2000Hz more
%sufficient [https://wiki.cimec.unitn.it/tiki-index.php?page=Data+pre-processing]

%Re-referencing: the data was probably online-referenced -> that's why no
%rereferencing in eeglab and also there is no such location-file of
%brain-vision

clear; clc;

[EEG, ALLEEG, CURRENTSET, LASTCOM, ALLCOM] = eeglab;

%cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC'; %working directory on mac
cd 'C:\Users\Amy Man Ji Cheung\Desktop\master-thesis\data\EC' %working directory on windows

eInfo = dir('*.set'); %just take the .set-files
ECepoch = {eInfo.name};

%[EEG, ALLEEG, CURRENTSET, LASTCOM, ALLCOM] = eeglab;
%Maybe have to initialize eeglab due to warning (but still works without
%though)

for i = 1:length(ECepoch)
    
    a = ECepoch{i};
    name = a(1:5);
    
    %for mac:
    %EEG = pop_loadset('filename', a,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC/');
    %for windows:
    EEG = pop_loadset('filename', a,'filepath','C:\Users\Amy Man Ji Cheung\Desktop\master-thesis\data\EC');
    %EEG = eeg_checkset( EEG );
    %EEG = pop_reref( EEG, []); %rereferencing with averaging methode
    EEG = eeg_checkset( EEG );
    EEG = pop_resample( EEG, 250); %resampling datasets from 5000Hz to 250Hz (eeglab automatically lowpass filters new dataset to half the sampling rate)
    EEG.setname= strcat(name, 'r'); %rr = 'rereferenced' & 'resampled' | 'r' = resampled
    EEG = eeg_checkset( EEG );
    %for mac:
    %EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC/');
    %for windows:
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','C:\Users\Amy Man Ji Cheung\Desktop\master-thesis\data\EC');
    EEG = eeg_checkset( EEG );
    
end


%Doing the same with EO-epochs

%cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO'; %working directory on mac
cd 'C:\Users\Amy Man Ji Cheung\Desktop\master-thesis\data\EO' %working directory on windows

eInfo = dir('*.set'); %just take the .set-files
EOepoch = {eInfo.name};


for i = 1:length(EOepoch)
    
    a = EOepoch{i};
    name = a(1:5);
    
    %for mac:
    %EEG = pop_loadset('filename', a,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO/');
    %for windows:
    EEG = pop_loadset('filename', a,'filepath','C:\Users\Amy Man Ji Cheung\Desktop\master-thesis\data\EO\');
    %EEG = eeg_checkset( EEG );
    %EEG = pop_reref( EEG, []); %rereferencing with averaging methode
    EEG = eeg_checkset( EEG );
    EEG = pop_resample( EEG, 250); %resampling datasets from 5000Hz to 250Hz (eeglab automatically lowpass filters new dataset to half the sampling rate)
    EEG.setname= strcat(name, 'r'); %rr = 'rereferenced' & 'resampled' 
    EEG = eeg_checkset( EEG );
    %for mac:
    %EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO/');
    %for windows:
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','C:\Users\Amy Man Ji Cheung\Desktop\master-thesis\data\EO\');
    EEG = eeg_checkset( EEG );
    
end


%Doing the same with Energy-epochs

%cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy'; %working directory on mac
cd 'C:\Users\Amy Man Ji Cheung\Desktop\master-thesis\data\Energy' %working directory on windows

eInfo = dir('*.set'); %just take the .set-files
Eepoch = {eInfo.name};

for i = 1:length(Eepoch)
    
    a = Eepoch{i};
    name = a(1:5);
    
    %for mac:
    %EEG = pop_loadset('filename', a,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy/');
    %for windows:
    EEG = pop_loadset('filename', a,'filepath','C:\Users\Amy Man Ji Cheung\Desktop\master-thesis\data\Energy\');
    %EEG = eeg_checkset( EEG );
    %EEG = pop_reref( EEG, []); %rereferencing with averaging methode
    EEG = eeg_checkset( EEG );
    EEG = pop_resample( EEG, 250); %resampling datasets from 5000Hz to 250Hz (eeglab automatically lowpass filters new dataset to half the sampling rate)
    EEG.setname= strcat(name, 'r'); %rr = 'rereferenced' & 'resampled' 
    EEG = eeg_checkset( EEG );
    %for mac:
    %EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy/');
    %for windows:
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','C:\Users\Amy Man Ji Cheung\Desktop\master-thesis\data\Energy\');
    EEG = eeg_checkset( EEG );
    
end


%Doing the same with Jeans-epochs

%cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans'; %working directory on mac
cd 'C:\Users\Amy Man Ji Cheung\Desktop\master-thesis\data\Jeans' %working directory on windows

eInfo = dir('*.set'); %just take the .set-files
Jepoch = {eInfo.name};

for i = 1:length(Jepoch)
    
    a = Jepoch{i};
    name = a(1:5);
    
    %for mac:
    %EEG = pop_loadset('filename', a,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/');
    %for windows:
    EEG = pop_loadset('filename', a,'filepath','C:\Users\Amy Man Ji Cheung\Desktop\master-thesis\data\Jeans\');
    EEG = eeg_checkset( EEG );
    %EEG = pop_reref( EEG, []); %rereferencing with averaging methode
    %EEG = eeg_checkset( EEG );
    EEG = pop_resample( EEG, 250); %resampling datasets from 5000Hz to 250Hz (eeglab automatically lowpass filters new dataset to half the sampling rate)
    EEG.setname= strcat(name, 'rr'); %rr = 'rereferenced' & 'resampled' 
    EEG = eeg_checkset( EEG );
    %for mac:
    %EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/');
    %for windows:
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','C:\Users\Amy Man Ji Cheung\Desktop\master-thesis\data\Jeans\');
    EEG = eeg_checkset( EEG );
    
end


%Section description: rereferencing data and downsample epochs to 250Hz

%% Filter Epochs
%till now epochs have been lowpass-filterd to 125Hz
%DC offset is already removed
%there is no phase-shift in eeglab (filtering forward and backward -> cancelles phase-shift)

%suggestion by stackoverflow
[b, a] = butter(order, cutoff/Fs, 'high');
data = filter(b, a, data);

%to remove line noise (50Hz) they recommend to use the eeglab plugin
%"Cleanline" -> also take a look on this paper:
%Widmann, Schröger, Maess, 2015. Digital filter design for
%electrophysiological data - a practical approach. J Neurosci Method.
%A short review and explanation of Cleanline: https://www.nitrc.org/projects/cleanline/


%todo: test if it's better to do a low-,high- and notch- filter or
%low-, high- and notch

%% testing

chanlocs = struct('labels', { 'Fp1' 'Fp2' 'F7' 'F8' 'F3' 'Fz' 'F4' 'FT7' 'FC3' 'FCz' 'FC4' 'FT8' 'T7' 'C3' 'Cz' 'C4' 'T8' 'TP9' 'TP7' 'CP3' 'CPz' 'CPz' 'CP4' 'TP8' 'TP10' 'P7' 'P3' 'Pz' 'P4' 'P8' 'O1' 'Oz' 'O2' }); 
pop_chanedit( chanlocs );

EEG.chanlocs = pop_chanedit(EEG.chanlocs, 'load',{ '/matlab/eeglab/sample_data/eeglab_chan32.locs', 'filetype', 'autodetect'});
figure; 
topoplot([],EEG.chanlocs, 'style', 'blank', 'electrodes', 'labelpoint');

%1) Edit -> Channel Location -> look up locs



