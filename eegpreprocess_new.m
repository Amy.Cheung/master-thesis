%% Preprocessing EEG-data

clear, clc;

cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles';

eInfo = dir('*.vhdr'); %just take the .vhdr-files
eegData = {eInfo.name}; %files are listed in each column

%[X] mkdir 'EEG'; Löschen: importierte EEG Daten werden nicht mehr gespeichert
    

[EEG, ALLEEG, CURRENTSET, LASTCOM, ALLCOM] = eeglab; %to initialize eeglab

for i = 1:length(eegData) %index for all the files
    
    a = eegData{i};
    name = a(3:5);
    
    %importing original raw eeg data
    EEG = pop_loadbv('/Users/amycheung/Desktop/Masterarbeit/audiofiles', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]); %choosing 32 channels because don't need the last two; 33 = EKG; 34 = GSR
    EEG.setname = strcat(name, 'EEG'); %saved the imported data
    EEG = eeg_checkset( EEG );
    
    %trying not to save the whole eeg file, because you can't read it
    %afterwards
    %EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles');
    %EEG = eeg_checkset( EEG );
    
    %extracting epoch EO
    %EO marker = 'S 83'
    %trial duration ~ 189sec
    EEG = pop_rmdat( EEG, {'S 83'},[0 189.05] ,0);
    EEG.setname= strcat(name, 'EO');
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1);%delete second 'boundary' marker
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO/');
    EEG = eeg_checkset( EEG );
    
end

%% Loading imported eeg data and continue epoching (EC)

%eList = dir(fullfile('EEG', '*.set')); %just take the .set-files in the EEG-folder
%eegData = {eList.name};

for i = 1:length(eegData)
    
    a = eegData{i};
    name = a(3:5);
    
    %load already imported original data (.set files) from the EEG folder
    
    EEG = pop_loadbv('/Users/amycheung/Desktop/Masterarbeit/audiofiles', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
    EEG.setname = strcat(name, 'EEG'); %saved the imported data
    EEG = eeg_checkset( EEG );
    
    %EEG = pop_loadset('filename', a ,'filepath', '/Users/amycheung/Desktop/Masterarbeit/audiofiles');
    %EEG = eeg_checkset( EEG );

    %extracting epoch EC
    %EO marker = 'S 81'
    %trial duration ~ 189sec
    EEG = pop_rmdat( EEG, {'S 81'},[0 189.05] ,0);
    EEG.setname= strcat(name, 'EC');
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1);%delete second 'boundary' marker
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC/');
    EEG = eeg_checkset( EEG );
    
end

%% Loading imported eeg data and continue epoching (Energy)

for i = 1:length(eegData)
    
    a = eegData{i};
    name = a(3:5);
    
    
    %loading original eeg data again
    
    EEG = pop_loadbv('/Users/amycheung/Desktop/Masterarbeit/audiofiles', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
    EEG.setname = strcat(name, 'EEG'); %saved the imported data
    EEG = eeg_checkset( EEG );
    
    %EEG = pop_loadset('filename', o,'filepath','/Users/amycheung/Documents/Masterarbeit/audiofiles/EEG/');
    %EEG = eeg_checkset( EEG );
    
    %extracting epoch Energy
    %EO marker = 'S 22'
    %trial duration ~ 373sec
    EEG = pop_rmdat( EEG, {'S 22'},[0 373.05] ,0);
    EEG.setname= strcat(name, 'Energy');
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1);%delete second 'boundary' marker
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','//Users//amycheung//Documents//Masterarbeit//audiofiles//Energy//');
    EEG = eeg_checkset( EEG ); %end file should contain 2 markers ('S 22' and 'S 52')
    
end


%% Loading imported eeg data and continue epoching (Jeans)

for i = 1:length(eegData)
    
    a = eegData{i};
    name = a(3:5);
    
    %loading orginal eeg data again
    
    EEG = pop_loadbv('/Users/amycheung/Desktop/Masterarbeit/audiofiles', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
    EEG.setname = strcat(name, 'EEG'); %saved the imported data
    EEG = eeg_checkset( EEG );
    
    %EEG = pop_loadset('filename', o,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles');
    %EEG = eeg_checkset( EEG );
    
    %extracting epoch Jeans
    %EO marker = 'S 23'
    %trial duration ~ 413.8sec
    EEG = pop_rmdat( EEG, {'S 23'},[0 413.85] ,0);
    EEG.setname= strcat(name, 'Jeans');
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1);%delete second 'boundary' marker
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/');
    EEG = eeg_checkset( EEG );
    
end

% [X]rmdir('/Users/amycheung/Documents/Masterarbeit/audiofiles/EEG', 's');
% braucht man nicht mehr da EEG-Ordner nicht existiert

%Script description:
%importing original data file and extracting trial epochs

%% Filter Epochs



