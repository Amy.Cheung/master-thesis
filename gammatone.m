cd ('/Volumes/lasthope/EEG/audiofiles');

audInfo = dir('*.wav'); %just take the .vhdr-files
audio = {audInfo.name};

%% 1. read audio signal


filename = 'Jeans SE_1.wav';
[y, fs] = audioread(filename); %y = audio signal; fs= sampling rate
info = audioinfo(filename);
timeArray = 0:seconds(1/fs):seconds(info.Duration);
timeArray = timeArray(1:end-1);
signalArray = y(:,1); %using signal data of the left ear (channel 1)


%audioMono = mean(y,2); %audio data converted into mono for easier visualization


%% 2. Create Gammatone-Filter for audio signal
%(GF filters represent representation of basilar membrane motion (BMM) as a function of time (BMM linear) [Patterson et al, 1988; Slaney 1993, Cooke, 1993] 
%-> more info on matlab "gammatone filter"
gammaFiltBank = gammatoneFilterBank('FrequencyRange',[250 8000], 'NumFilters', 128, 'SampleRate', fs); %numbers according to Crosse (2016)
audioSamp = resample(signalArray, 250, 48000); %downsample audio data
audioOut = gammaFiltBank(audioSamp);
[N,numChannels] = size(audioOut);

%% audio mean test

audioMean = [];
b = (0:8:128);
b(1) = 1;

for c = 1:16
    aud = audioOut(:,(b(c)):(b(c + 1))); 
    
    for a = 1:length(audioOut)
        audioMean(a,c) = mean(aud(a, :));
    end
end


%% 3. Normalize audio & eeg
    %2 kinds of normalizing:
    %a)normalize data before spliting into train and test => testing validates
    %model/structure of data (i.e. important variables)
    %b)use mean and std for test set => focus on predictive algorithm

    %Audio (should already be loaded in previous section)
    audioNormed = audioOut/std(audioOut(:)); %normalizing audio-data
    audioMeanNormed = audioMean/std(audioMean(:));
    
%EEG
cd '/Volumes/lasthope/EEG/Clean/Energy_clean/Pro/SE';
intInfo = dir('*.set');
int = {intInfo.name};

%se = struct('c', {'/Volumes/G-DRIVE mobile USB-C/EEG/Clean/Energy_clean/Pro/SE', '/Volumes/G-DRIVE mobile USB-C/EEG/Clean/Energy_clean/Stu/SE', '/Volumes/G-DRIVE mobile USB-C/EEG/Clean/Energy_clean/MulPro/SE', '/Volumes/G-DRIVE mobile USB-C/EEG/Clean/Energy_clean/MulStu/SE'});

for i = 1:length(int)
    
eeg = pop_loadset('filename', int(i) ,'filepath', '/Volumes/lasthope/EEG/Clean/Energy_clean/Pro/SE');           
eegData = eeg.data;
eegData = reshape(eegData, [length(eegData),32]); %changing into long format
eegShort = eegData(1:93552, :); 
eegNormed = eegShort/std(eegShort(:)); %normalizing eeg-data

end




%% 4. Generate training/test sets
%stim = audio
%resp = eeg

stim = sum(audioNormed,2); % Sums up the 16 audio-band = mean-audio-band
eegDouble = double(eegNormed);
fs = 250;

nfold = 180;
testfold = 1;
[stimtrain,resptrain,stimtest,resptest] = mTRFpartition(stim,eegDouble,nfold,testfold);

% Model hyperparameters
tmin = 0;
tmax = 250;
lambdas = 10.^(-10:2:10);

cv = mTRFcrossval(stimtrain,resptrain,fs,-1,tmin,tmax,lambdas,...
    'zeropad',0, 'fast', 0);


%% Get optimal hyperparameters
[rmax,idx] = max(mean(cv.r));
lambda = lambdas(idx);
nlambda = length(lambdas);

%% Train model
model = mTRFtrain(stimtrain,resptrain,fs,-1,tmin,tmax,lambda,'zeropad',0);

%% ---Model testing---

% Test model
[pred,test] = mTRFpredict(stimtest,resptest,model,'zeropad',0);

%% Crosse stimulus reconstruction (WORKS!)

clear; clc;

%cd '/Volumes/lasthope/EEG/audiofiles/finAudio/EE';
%audio = dir('*.mat');


cd ('/Volumes/lasthope/EEG/fin/EO/mPro');
intInfo = dir('*.set');
int = {intInfo.name};

cd '/Volumes/lasthope/EEG/audiofiles';
audInfo = dir('*.wav');
audio = {audInfo.name};
% 
%
[y, fs] = audioread(audInfo(3).name); %y = audio signal; fs= sampling rate
info = audioinfo(audInfo(3).name);
timeArray = 0:seconds(1/fs):seconds(info.Duration);
timeArray = timeArray(1:end-1);
signalArray = y(:,1); %using signal data of the left ear (channel 1)

audioSamp = resample(signalArray, 250, 48000); %downsample audio data
audioSamprem = audioSamp;


%for p = 1%:length(aud)
    
%     cd '/Volumes/lasthope/EEG/audiofiles/finAudio/EE';
%     load(audio(p).name);
    
    pred = [];
    test = [];

    for i = 1:length(int)
        
        
        %audioSamp = EnElfAudioStu(i).data;
        audioSamp = audioSamprem;
        
        
        fs = 48000;
        gammaFiltBank = gammatoneFilterBank('FrequencyRange',[250 8000], 'NumFilters', 128, 'SampleRate', fs); %numbers according to Crosse (2016)
        % audioSamp = resample(signalArray, 250, 48000); %downsample audio data
        audioOut = gammaFiltBank(audioSamp);
        %[N,numChannels] = size(audioOut);
        
        audioOut = hilbert(audioOut);
        % Average every 8 neighbouring bands of the filtered audio signal -> 128/8=16
        
        audioMean = [];
        b = (0:8:128);
        b(1) = 1;
        
        for c = 1:16
            aud = audioOut(:,(b(c)):(b(c + 1)));
            
            for a = 1:length(audioOut)
                audioMean(a,c) = mean(aud(a, :));
            end
        end
        
        
        audioNormed = audioMean/std(audioMean(:));
        
        %Downsample audio
        fs = 250;
        fsNew = 64;
        audioNormed = resample(audioNormed,fsNew,fs);
        
        nfold = 180;
        testfold = 1;
        
        tmin = 0;
        tmax = 250;
        lambdas = 10.^(-10:2:10);
       
        
        %for i = 1%:((length(int))-1)
        cd ('/Volumes/lasthope/EEG/fin/EO/mPro');
        
        eeg = pop_loadset('filename', int(i) ,'filepath', '/Volumes/lasthope/EEG/fin/EO/mPro');
        eegData = eeg.data;
        eegData = double(eegData);
        eegData = reshape(eegData, [length(eegData),32]);
        eegData = resample(eegData,fsNew, 250);
        fs = fsNew;
        
        if length(eegData) > length(audioNormed)
            %eegData = reshape(eegData, [length(eegData),32]);
            eegShort = eegData(1:(length(audioNormed)),:);
            eegNormed = eegShort/std(eegShort(:));
            stim = sum(audioNormed,2);
            
        elseif length(eegData) < length(audioNormed)
            audioShort = audioNormed(1:(length(eegData)),:);
            eegNormed = eegData/std(eegData(:));
            stim = sum(audioShort,2);
            
        else
            audioShort = audioNormed(1:(length(eegData)), :);
            %eegData = reshape(eegData, [length(eegData),32]);
            eegNormed = eegData/std(eegData(:));
            stim = sum(audioShort,2);
        end
        
        
        eegDouble = double(eegNormed);

        
        [stimtrain,resptrain,stimtest,resptest] = mTRFpartition(stim,eegDouble,nfold,testfold);
        
        cv = mTRFcrossval(stimtrain,resptrain,fs,-1,tmin,tmax,lambdas,...
            'zeropad',0, 'fast', 0);
        
        % --- Model training --- %
        
        % Get optimal hyperparameters
        [rmax,idx] = max(mean(cv.r));
        lambda = lambdas(idx);
        nlambda = length(lambdas);
        
        % Train model
        model = mTRFtrain(stimtrain,resptrain,fs,-1,tmin,tmax,lambda,'zeropad',0);
        
        % --- Model testing ---
        
        % Test model
        [pred{i}, test{i}] = mTRFpredict(stimtest,resptest,model,'zeropad',0);
        
    end
%end

for zz = 1:length(test)
rTotal(zz) = (test{1,zz}.r);
errTotal(zz) = (test{1,zz}.err);
end

rEnd = mean(rTotal);
errEnd = mean(errTotal);
rEnd
errEnd

%% test
[upperEnvelope, lowerEnvelope] = envelope(stim, 250, 'peak');
timeSamp = linspace((timeArray(1,1)), (timeArray(1, end)), (length(upperEnvelope)));
plot(timeSamp, upperEnvelope);

[eegUpEnv, eeglowEnv] = envelope((eegDouble(:,1)), 250, 'peak');
[audioMeanUpEnv, audioMeanLowEnv] = envelope(meanAudio, 250, 'peak');

for q = 1:length(eegDouble)
    meanEEG(q) = mean(eegDouble(q,1:32));
end
meanEEG = meanEEG';
[meanEEGenvUP, meanEEGenvLow] = envelope(meanEEG, 250, 'peak');

for q = 1:length(audioShort)
    meanAudio(q) = mean(audioShort(q,1:16));
end
meanAudio = meanAudio';

figure;
hold on;
plot(timeSamp, upperEnvelope);
plot(timeSamp, meanEEGenvUP);

figure;
hold on;
plot(timeSamp, meanEEGenvUP);
plot(timeSamp, audioMeanUpEnv);


%% linear model test

for q = 1:32
mdl = fitlm((eegDouble(:,q)),upperEnvelope,'linear');

end

r = [];
for q =1:32
    R = corrcoef(eegDouble(:,q),stim);
    r(q) = R(2);
    
end
