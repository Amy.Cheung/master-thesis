%% Test Phase

eegData = reshape(eegData, [length(eegData),32]);
eegData = eegData(1:103622, 1:32);
signalResampled = resample(signalArray, 250, fs);
[upperEnvelope, lowerEnvelope] = envelope(signalResampled, 250, 'peak');
EEGdata = eegData(1:103622, 1:32);

figure;
plot(eegTimes(1,1:103622),upperEnvelope);

[r,m,b] = regression(EEGdata, signalResampled);

plotregression(EEGdata, signalResampled);
plotregression(EEGdata, upperEnvelope);

%how to schnibble EEG data in 5sec pieces
eegTime = EEG.times/1000; %unit for EEG.times is in milliseconds (x/1000 = seconds)
eegTime = eegTime(1,1:103622);
A = 0:417;
T = A(1:5:EEG.xmax);

%finds rows with 5sec spacing
for t = 1:(length(T)-1)
    p(t) = find(eegTime == T(t));
end

%%%% EEG Schnipseln into 5seconds pieces
cd /Users/amycheung/Desktop/Masterarbeit/audiofiles/Train
fn = 'eegDataSnip.mat';

for i = 1:(length(p)-1)
    %name = [num2str(i),'eegDataSnip.mat'];
    E(i) = {eegData((p(i):p(i+1)),1:32)};
     
    save('eegSnips', 'E');
   
end



%steps 1250
%%%%%



eegNewTime = [];
for i = 1:length(p)
    eegNewTime(i) = eegTimes(1, p(i));
    
end

figure;
hold on;
plot(eegTimes,eegData(:,1));
xline(eegTimes(1,p(1)));

for i = length(p)
    xline(eegTimes(1, p(i)));
end



%% (WORKING) Separating cleaned ICA data into 5sec pieces

f = struct('c', {'/Users/amycheung/Desktop/Masterarbeit/audiofiles/ICA/EC', ...
    '/Users/amycheung/Desktop/Masterarbeit/audiofiles/ICA/EO', ...
    '/Users/amycheung/Desktop/Masterarbeit/audiofiles/ICA/Energy', ...
    '/Users/amycheung/Desktop/Masterarbeit/audiofiles/ICA/Jeans'}) ;

sav = struct('c', {'/Users/amycheung/Desktop/Masterarbeit/audiofiles/Train/EC', ...
    '/Users/amycheung/Desktop/Masterarbeit/audiofiles/Train/EO', ...
    '/Users/amycheung/Desktop/Masterarbeit/audiofiles/Train/Energy', ...
    '/Users/amycheung/Desktop/Masterarbeit/audiofiles/Train/Jeans'}) ;

p = []; %initializing p; p is finding the exact row of 5sec

for o = 1:length(f) %loops through the ICAed folders
    d = f(o).c;
    cd (d);
    
    icaInfo = dir('*.set');
    icaData = {icaInfo.name};
    nfiles = size(icaData,2); %length of all ica data sets
    
    for z = 1:nfiles
        EEG = pop_loadset('filename', icaData{z},'filepath', d); %1. load in file
        EEG = eeg_checkset( EEG );
        eegTime = EEG.times/1000; %unit for EEG.times is in milliseconds (x/1000 = seconds)
        A = 0:eegTime(end);
        T = A(1:5:EEG.xmax); %5sec steps
        
        %finds 5,10,15sec,... rows
        for t = 1:(length(T))
            p(t) = find(eegTime == T(t));
        end
        
        eegData = EEG.data;
        eegData = reshape(eegData, [length(eegData),32]); %changing into long format
        if length(eegData) > 48251
            eegData = eegData(1:103622, 1:32);
            
        else
            continue
        end
            
        
        
        %E(1) = {eegData(1:(p(2)), 1:32)};
        
        for j = 1:(length(p)-1)
            E(j) = {eegData((p(j)):(p(j+1)),1:32)};
            
        end
        
        cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/Train';
        ic = icaData{z};
        ic = ic(1:5);
        save(strcat(ic, '_eegSnips'), 'E');
        
        
        
        %             for q = 1:length(f)
        %                 if q = 1
        %                     ic = icaData{1};
        %                     ic = ic(1:5);
        %                     save(strcat(ic, '_eegSnips'), 'E');
        %
        %                 end
        %             end
        
        
    end
    
end
   

% working!

%% Segmenting Envelope Data

envSamp = upperEnvelope/250;
envSamp = resample(upperEnvelope, 250, fs); %Jeans Elf Envelope

[upperEnvelope, lowerEnvelope] = envelope(signalArray, 250, 'peak'); %for Jeans Elf


[yEn, fsEn] = audioread('Energy Advisors ELF_1.wav');
infoEn = audioinfo('Energy Advisors ELF_1.wav');
timeArrayEn = 0:seconds(1/fsEn):seconds(infoEn.Duration);
timeArrayEn = timeArrayEn(1:end-1);
signalArrayEn = yEn(:,1); %using signal data of the left ear (channel 1)

[upperEnvelopeEn, lowerEnvelopeEn] = envelope(signalArrayEn, 250, 'peak'); %for Energy Elf
envEn = resample(upperEnvelopeEn, 250, fsEn);


%Note:
%Energy Elf signal is much shorter than the Jeans Elf
%Q: is the EEG data of people with Energy Elf condition also shorter than
%others with the Jeans condition?

%% Test Phase II
figure;
plot(mvregress(EEGdata, upperEnvelope))
hold on
xlabel('EEG data');
ylabel('upper Audio Envelope');

figure;
plot(EEGdata, upperEnvelope);
hold on
xlabel('EEG data');
ylabel('upper Audio Envelope');


%% Linear Regression

[x,t] = simplefit_dataset;
net = feedforwardnet(20);
net = train(net,x,t);
y = net(x);
[r,m,b] = regression(t,y)


%% Ridge Regression



%% Weighted Ridge Regression





%% Test Window

EEGdata = zeros(1:32);

for i = [1:32]
    EEGdata{i} = eegData(:, i);
    
end

eegStacked = [EEGdata1; EEGdata2; EEGdata3; EEGdata4; EEGdata5; EEGdata6; EEGdata7; EEGdata8; EEGdata9; EEGdata10;...
    EEGdata11; EEGdata12; EEGdata13; EEGdata14; EEGdata15; EEGdata16; EEGdata17; EEGdata18; EEGdata19; EEGdata20; ...
    EEGdata21; EEGdata22; EEGdata23; EEGdata24; EEGdata25; EEGdata26; EEGdata27; EEGdata28; EEGdata29; EEGdata30; ...
    EEGdata31; EEGdata32];

EEGdata = struct({EEGdata1, EEGdata2, EEGdata3, EEGdata4, EEGdata5, EEGdata6, EEGdata7, EEGdata8, EEGdata9, EEGdata10,...
    EEGdata11, EEGdata12, EEGdata13, EEGdata14, EEGdata15, EEGdata16, EEGdata17, EEGdata18, EEGdata19, EEGdata20, ...
    EEGdata21, EEGdata22, EEGdata23, EEGdata24, EEGdata25, EEGdata26, EEGdata27, EEGdata28, EEGdata29, EEGdata30, ...
    EEGdata31, EEGdata32})

Channels = {'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23',};
