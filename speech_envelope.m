%% Read in Audio Files

clear; clc;

cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles';

filename = 'Energy Advisors SE_1.wav';
[y, fs] = audioread(filename); %y = audio signal; Fs= sampling rate

info = audioinfo(filename);
timeArray = 0:seconds(1/fs):seconds(info.Duration);
timeArray = timeArray(1:end-1);
signalArray = y(:,1); %using signal data of the left ear (channel 1)

[upperEnvelope, lowerEnvelope] = envelope(signalArray, 250, 'peak');
%creating upper and lower envelope of the audio signal (all frequencies)
%samplig at 500 (still needs to find the optimum)
%peak = after each 500th datapoint, use the highest amplitude

figure;
%hold on;
plot(timeArray,signalArray); %plot raw audio signal
title('raw Audio Signal');

I = seconds(info.Duration); %Divides Audio Signal Plot into 15sec Pieces
for z = 1:28
xline(seconds(z*15), 'r');
hold on;
end


figure;
hold on;
plot(timeArray, upperEnvelope); %plot upper envelope
plot(timeArray, lowerEnvelope); %plot lower envelope

figure;
[a,b,c] = spectrogram(signalArray,[],[],[], fs, 'yaxis'); %plot spectrogram (power of frequencies)

%% Test Envelope
[upperEnvelope2, lowerEnvelope2] = envelope(signalArray, 2400, 'peak');
%Testing Envelope, if sampling rate is higher (2400)

figure;
hold on;
plot(timeArray, upperEnvelope2); %plot upper speech envelope
plot(timeArray, lowerEnvelope2); %plot lower speech envelope


%% Spectral Analysis

% FOURIER TRANSFORMATION

ft = fft(signalArray); %fourier transformation of onesided signal

n = length(signalArray); %signal size
f = (0:n-1)*(fs/n); %frequency range
power = abs(ft).^2/n; %power of the DFT
nyquist = fs/2;

figure;
plot(f,power); %plot power spectrum
grid on;
xlabel('Frequency');
ylabel('Power');
xline(nyquist, 'r'); %creates midpoint (nyquist)
%result: frequencies are mirrored (positive and negative frequencies);
%middle = nyquist (Fs/2)


%% Lowpass-Filtering 0-30Hz


signalArrayFiltered = lowpass(signalArray, 30, fs,'Steepness', 0.95); %lowpass filter signal to 30Hz
figure;
plot(timeArray,signalArrayFiltered); %looks/sounds the same as audio signal


% ftFiltered = fft(signalArrayFiltered);
% powerFiltered = abs(ftFiltered).^2/n;
% figure;
% plot(f,powerFiltered);


spectrogram(signalArrayFiltered,[],[],[], fs, 'yaxis')
title('Spectrogramm of Lowpass Filtered Signal')

%% Butterworth Filter of Audio Signal
%8th-order lowpass Butterworth filter

freqCutOff = 30

[g,h] = butter(8, freqCutOff/(fs/2));
freqz(g,h);

filtOut = filter(g,h, signalArray);
plot(timeArray, filtOut);

%% Lowpass Filtering of Audio Signal to 30Hz

lpFilt = designfilt('lowpassfir', 'FilterOrder', 8, 'CutoffFrequency', 0.3, 'SampleRate', fs);
lpFiltSignal = filter(lpFilt, signalArray); %using the lowpass filter on the signal array

figure;
%plot(timeArray, lpFiltSignal);
plot(timeArray, signalArray); hold on; plot(timeArray, lpFiltSignal);
title('Audio Signal vs Low Pass filtered Signal');

lpFiltSignal = filtfilt(lpFilt, signalArray); %correcting the delay in the signal (zero-phase-shift)
figure;
plot(timeArray, signalArray); hold on; plot(timeArray, lpFiltSignal) %blue: signalArray; orange: filtered signal
title('Audio Signal vs Zero-Phase-Shift LP Signal');

detrendlpFiltSignal = detrend(lpFiltSignal); %removes polynominial trend
figure;
plot(timeArray, signalArray); hold on; plot(timeArray, detrendlpFiltSignal)
title('Audio Signal vs detrend ZPS LP Signal');

%% Downsample Speech Signal

downSignal = downsample(signalArrayFiltered, 100);
plot(timeArrayDown,downSignal);

% Plot Speech Envelope of the filtered and downsampled signal
fsDown = 480.0008;
timeArrayDown = 0:seconds(1/fsDown):seconds(info.Duration);
timeArrayDown = timeArrayDown(1:end-1);


[upperEnvelopeDown, lowerEnvelopeDown] = envelope(downSignal, 20, 'peak');

figure;
plot(timeArrayDown, upperEnvelopeDown);
title('upper Speech Envelope of filtered and downsampled Signal')

figure;
plot(timeArrayDown, lowerEnvelopeDown);

%% Test: Crop audio file into 10s snippets

% snip = mirframe(signalArray, 'Length', 60, 's', '/l', 0);
% s = mirspectrum(snip);
% 
% snip = mirframe(signalArray, 'Hop', 0, 's');

cd '/Volumes/Daten/CLINT/06_Praktikanten/Amy_Masterarbeit/Stimuli/Soundfiles/Energy Advisors ELF_1';

filename1 = 'Energy Advisors ELF_1(0-15).wav';
[y1, fs1] = audioread(filename1); %y = audio signal; Fs= sampling rate

info = audioinfo(filename1);
timeArray1 = 0:seconds(1/fs1):seconds(info.Duration);
timeArray1 = timeArray1(1:end-1);
signalArray1 = y1(:,1); %using signal data of the left ear (channel 1)

figure;
%hold on;
plot(timeArray1,signalArray1); %plot raw audio signal
title('raw Audio Signal');

