%% Preprocessing EEG-data

clear, clc;

cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles';

eInfo = dir('*.vhdr'); %just take the .vhdr-files
eegData = {eInfo.name}; %files are listed in each column

%[X] mkdir 'EEG'; Löschen: importierte EEG Daten werden nicht mehr gespeichert
    

[EEG, ALLEEG, CURRENTSET, LASTCOM, ALLCOM] = eeglab; %to initialize eeglab
%maybe do not even have to initialize eeglab to import and process data

for i = 1:length(eegData) %index for all the files
    
    a = eegData{i};
    name = a(3:5);
    %name = a(5:7) -> for the real data: example "MPR_DTH" (take the 5:7)
    
    %importing original raw eeg data
    EEG = pop_loadbv('/Users/amycheung/Desktop/Masterarbeit/audiofiles', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]); %choosing 32 channels because don't need the last two; 33 = EKG; 34 = GSR
    EEG.setname = strcat(name, 'EEG'); %saved the imported data
    EEG = eeg_checkset( EEG );

    
    %trying not to save the whole eeg file, because you can't read it
    %afterwards
    %EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles');
    %EEG = eeg_checkset( EEG );
    
    %extracting epoch EO
    %EO marker = 'S 83'
    %trial duration ~ 189sec (adding additional 2sec for buffer when
    %filtering data)
    EEG = pop_rmdat( EEG, {'S 83'},[-2 191] ,0);
    EEG.setname= strcat(name, 'EO');
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1);%delete second 'boundary' marker
    EEG = eeg_checkset( EEG );
    %setting channel locations
    EEG=pop_chanedit(EEG, 'lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp','lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp');
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO/');
    EEG = eeg_checkset( EEG );
    
end

%% Loading imported eeg data and continue epoching (EC)

%eList = dir(fullfile('EEG', '*.set')); %just take the .set-files in the EEG-folder
%eegData = {eList.name};

for i = 1:length(eegData)
    
    a = eegData{i};
    name = a(3:5);
    %name = a(5:7) -> for the real data: example "MPR_DTH" (take the 5:7)
    
    %load already imported original data (.set files) from the EEG folder
    
    EEG = pop_loadbv('/Users/amycheung/Desktop/Masterarbeit/audiofiles', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
    EEG.setname = strcat(name, 'EEG'); %saved the imported data
    EEG = eeg_checkset( EEG );
    
    %EEG = pop_loadset('filename', a ,'filepath', '/Users/amycheung/Desktop/Masterarbeit/audiofiles');
    %EEG = eeg_checkset( EEG );

    %extracting epoch EC
    %EO marker = 'S 81'
    %trial duration ~ 189sec (adding additional 2sec for buffer when
    %filtering data)
    EEG = pop_rmdat( EEG, {'S 81'},[-2 191] ,0);
    EEG.setname= strcat(name, 'EC');
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1);%delete second 'boundary' marker
    EEG = eeg_checkset( EEG );
    %setting channel locations
    EEG=pop_chanedit(EEG, 'lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp','lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp');
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC/');
    EEG = eeg_checkset( EEG );
    
end

%% Loading imported eeg data and continue epoching (Energy)

for i = 1:length(eegData)
    
    a = eegData{i};
    name = a(3:5);
    %name = a(5:7) -> for the real data: example "MPR_DTH" (take the 5:7)
    
    %loading original eeg data again
    
    EEG = pop_loadbv('/Users/amycheung/Desktop/Masterarbeit/audiofiles', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
    EEG.setname = strcat(name, 'EEG'); %saved the imported data
    EEG = eeg_checkset( EEG );
    
    %EEG = pop_loadset('filename', o,'filepath','/Users/amycheung/Documents/Masterarbeit/audiofiles/EEG/');
    %EEG = eeg_checkset( EEG );
    
    %extracting epoch Energy
    %EO marker = 'S 22'
    %trial duration ~ 373sec (adding additional 2sec for buffer when
    %filtering data)
    EEG = pop_rmdat( EEG, {'S 22'},[-2 373.65] ,0);
    EEG.setname= strcat(name, 'Energy');
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1); %delete second 'boundary' marker
    EEG = eeg_checkset( EEG );
    %setting channel locations
    EEG=pop_chanedit(EEG, 'lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp','lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp');
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy/');
    EEG = eeg_checkset( EEG ); %end file should contain 2 markers ('S 22' and 'S 52')
    
end


%% Loading imported eeg data and continue epoching (Jeans)

for i = 1:length(eegData)
    
    a = eegData{i};
    name = a(3:5);
    %name = a(5:7) -> for the real data: example "MPR_DTH" (take the 5:7)
    
    %loading orginal eeg data again
    
    EEG = pop_loadbv('/Users/amycheung/Desktop/Masterarbeit/audiofiles', a, [ ], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]);
    EEG.setname = strcat(name, 'EEG'); %saved the imported data
    EEG = eeg_checkset( EEG );
    
    %EEG = pop_loadset('filename', o,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles');
    %EEG = eeg_checkset( EEG );
    
    %extracting epoch Jeans
    %EO marker = 'S 23'
    %trial duration ~ 413.8sec (adding additional 2sec for buffer when
    %filtering data)
    EEG = pop_rmdat( EEG, {'S 23'},[-2 415] ,0);
    EEG.setname= strcat(name, 'Jeans');
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1); %delete 'boundary' marker
    EEG = eeg_checkset( EEG );
    EEG = pop_editeventvals(EEG,'delete',1);%delete second 'boundary' marker
    EEG = eeg_checkset( EEG );
    %setting channel locations
    EEG=pop_chanedit(EEG, 'lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp','lookup','/Users/amycheung/Desktop/Masterarbeit/MATLAB/eeglab2019_1/plugins/dipfit/standard_BESA/standard-10-5-cap385.elp');
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename', EEG.setname,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/');
    EEG = eeg_checkset( EEG );
    
end

%Script description:
%importing original data file and extracting trial epochs

%% Referencing & Downsampling
%used average reference:
%Average Energy on the scalp (sum all the electrodes) should be 0

%Downsampling the epochs to 250Hz
%-> Matlab will automatically lowpass filter the dataset to the half of the
%sampling rate. For example 250Hz -> 125Hz lowpass filter
%250Hz = 1 data point per 4ms
%--> sampling rate can't be too low because of aliasing (Nyquist theory: at
%least 2times higher than frequency of interest)
%depending on research goal a higher sampling rate is more fitted, for
%example high oscillatory dynamics (30-100Hz) -> 1000Hz/2000Hz more
%sufficient [https://wiki.cimec.unitn.it/tiki-index.php?page=Data+pre-processing]

%Re-referencing: the data was probably online-referenced -> that's why no
%rereferencing in eeglab and also there is no such location-file of
%brain-vision


%[EEG, ALLEEG, CURRENTSET, LASTCOM, ALLCOM] = eeglab; doesn't need to
%initialize again


b = struct('c', {'/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/', '/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy/','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC', '/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO/'});

for i = 1:length(b)
    
    d = b(i).c
    cd (d);
    
    eInfo = dir('*.set'); %just take the .set-files
    Epoch = {eInfo.name};
    
    for j = 1:length(Epoch)
        
        a = Epoch{j};
        name = a(1:5);
        
        EEG = pop_loadset('filename', a,'filepath', d);
        %EEG = eeg_checkset( EEG );
        %EEG = pop_reref( EEG, []); %rereferencing with averaging methode
        EEG = eeg_checkset( EEG );
        EEG = pop_resample( EEG, 250); %resampling datasets from 5000Hz to 250Hz (eeglab automatically lowpass filters new dataset to half the sampling rate)
        EEG.setname= strcat(name, 'r'); %rr = 'rereferenced' & 'resampled'
        EEG = eeg_checkset( EEG );
        
        
        %Filtering sampled data
        EEG = pop_eegfiltnew(EEG, 'locutoff',0.1,'plotfreqz',0);
        EEG = eeg_checkset( EEG );
        
        %notch filter (50Hz) Line Noise
        EEG = pop_eegfiltnew(EEG, 'locutoff',49.5,'hicutoff',50.5,'revfilt',1,'plotfreqz',0);
        EEG = eeg_checkset( EEG );
        
        %low-pass filtering
        %     EEG = pop_eegfiltnew(EEG, 'hicutoff',30, 'plotfreqz',1);
        %     EEG.setname= strcat(name, '_filt');
        %     EEG = eeg_checkset( EEG );
        
        EEG.setname= strcat(name, '_filt');
        EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/filt');
        EEG = eeg_checkset( EEG );

    end
end


cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC';

eInfo = dir('*.set'); %just take the .set-files
ECepoch = {eInfo.name};

%[EEG, ALLEEG, CURRENTSET, LASTCOM, ALLCOM] = eeglab;
%Maybe have to initialize eeglab due to warning (but still works without
%though)

for i = 1:length(ECepoch)
    
    a = ECepoch{i};
    name = a(1:5);
    
    EEG = pop_loadset('filename', a,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC/');
    %EEG = eeg_checkset( EEG );
    %EEG = pop_reref( EEG, []); %rereferencing with averaging methode
    EEG = eeg_checkset( EEG );
    EEG = pop_resample( EEG, 250); %resampling datasets from 5000Hz to 250Hz (eeglab automatically lowpass filters new dataset to half the sampling rate)
    EEG.setname= strcat(name, 'r'); %rr = 'rereferenced' & 'resampled' 
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC/');
    EEG = eeg_checkset( EEG );
    
end


%Doing the same with EO-epochs

cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO';

eInfo = dir('*.set'); %just take the .set-files
EOepoch = {eInfo.name};


for i = 1:length(EOepoch)
    
    a = EOepoch{i};
    name = a(1:5);
    
    EEG = pop_loadset('filename', a,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO/');
    %EEG = eeg_checkset( EEG );
    %EEG = pop_reref( EEG, []); %rereferencing with averaging methode
    EEG = eeg_checkset( EEG );
    EEG = pop_resample( EEG, 250); %resampling datasets from 5000Hz to 250Hz (eeglab automatically lowpass filters new dataset to half the sampling rate)
    EEG.setname= strcat(name, 'r'); %rr = 'rereferenced' & 'resampled' 
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO/samp');
    EEG = eeg_checkset( EEG );
    
end


%Doing the same with Energy-epochs

cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy';

eInfo = dir('*.set'); %just take the .set-files
Eepoch = {eInfo.name};

for i = 1:length(Eepoch)
    
    a = Eepoch{i};
    name = a(1:5);
    
    EEG = pop_loadset('filename', a,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy/');
    %EEG = eeg_checkset( EEG );
    %EEG = pop_reref( EEG, []); %rereferencing with averaging methode
    EEG = eeg_checkset( EEG );
    EEG = pop_resample( EEG, 250); %resampling datasets from 5000Hz to 250Hz (eeglab automatically lowpass filters new dataset to half the sampling rate)
    EEG.setname= strcat(name, 'r'); %rr = 'rereferenced' & 'resampled' 
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy/');
    EEG = eeg_checkset( EEG );
    
end


%Doing the same with Jeans-epochs

cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans';

eInfo = dir('*.set'); %just take the .set-files
Jepoch = {eInfo.name};

for i = 1:length(Jepoch)
    
    a = Jepoch{i};
    name = a(1:5);
    
    EEG = pop_loadset('filename', a,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/');
    EEG = eeg_checkset( EEG );
    %EEG = pop_reref( EEG, []); %rereferencing with averaging methode
    %EEG = eeg_checkset( EEG );
    EEG = pop_resample( EEG, 250); %resampling datasets from 5000Hz to 250Hz (eeglab automatically lowpass filters new dataset to half the sampling rate)
    EEG.setname= strcat(name, 'r'); %rr = 'rereferenced' & 'resampled' 
    EEG = eeg_checkset( EEG );
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/');
    EEG = eeg_checkset( EEG );
    
end


%Section description: rereferencing data and downsample epochs to 250Hz

%% Filter Epochs
%till now epochs have been lowpass-filterd to 125Hz
%DC offset is already removed
%there is no phase-shift in eeglab (filtering forward and backward -> cancelles phase-shift)

%suggestion by stackoverflow
% [b, a] = butter(order, cutoff/Fs, 'high');
% data = filter(b, a, data);

%to remove line noise (50Hz) they recommend to use the eeglab plugin
%"Cleanline" -> also take a look on this paper:
%Widmann, Schröger, Maess, 2015. Digital filter design for
%electrophysiological data - a practical approach. J Neurosci Method.
%A short review and explanation of Cleanline: https://www.nitrc.org/projects/cleanline/

% EEG = pop_eegfilt( EEG, 1, 0); %filtering eeg data without popping GUI


%%% Filtering EO data sets %%% --> 

cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO/samp';

eInfo = dir('*.set'); %just take the .set-files
EOsamp = {eInfo.name};


for i = 1:length(EOsamp)
    
    a = EOsamp{i};
    name = a(1:5);
    
    %high-pass filtering
    EEG = pop_loadset('filename', a,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO/samp/');
    EEG = pop_eegfiltnew(EEG, 'locutoff',0.1,'plotfreqz',0);
    EEG = eeg_checkset( EEG );
    
    %notch filter (50Hz) Line Noise
    EEG = pop_eegfiltnew(EEG, 'locutoff',49.5,'hicutoff',50.5,'revfilt',1,'plotfreqz',0);
    EEG = eeg_checkset( EEG );
    
    %low-pass filtering
    %     EEG = pop_eegfiltnew(EEG, 'hicutoff',30, 'plotfreqz',1);
    %     EEG.setname= strcat(name, '_filt');
    %     EEG = eeg_checkset( EEG );
    
    EEG.setname= strcat(name, '_filt');
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO/filt');
    EEG = eeg_checkset( EEG );
    
end



%%% Filtering EC data sets %%% --> filtering good

cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC/samp';

eInfo = dir('*.set'); %take the resampled EC.set files in the samp folder
ECsamp = {eInfo.name};

for i = 1:length(ECsamp)
    
    a = ECsamp{i};
    name = a(1:5);
    
    %high-pass filtering
    EEG = pop_loadset('filename', a,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC/samp/');
    EEG = pop_eegfiltnew(EEG, 'locutoff',0.1,'plotfreqz',0);
    EEG = eeg_checkset( EEG );
    
    %notch filter (50Hz) Line Noise
    EEG = pop_eegfiltnew(EEG, 'locutoff',49.5,'hicutoff',50.5,'revfilt',1,'plotfreqz',0);
    EEG = eeg_checkset( EEG );
    %best filter frequencies: 49.45-50.55Hz
    
    %low-pass filtering
    %     EEG = pop_eegfiltnew(EEG, 'hicutoff',30, 'plotfreqz',0);
    %     EEG.setname= strcat(name, '_filt');
    %     EEG = eeg_checkset( EEG );
    
    EEG.setname= strcat(name, '_filt');
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC/filt');
    EEG = eeg_checkset( EEG );
    
end



%%% Filtering Energy data sets %%% --> filtering good

cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy/samp';

eInfo = dir('*.set'); %take the resampled Energy.set files in the samp folder
Ensamp = {eInfo.name};

for i = 1:length(Ensamp)
    
    a = Ensamp{i};
    name = a(1:5);
    
    %high-pass filtering
    EEG = pop_loadset('filename', a,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy/samp/');
    EEG = pop_eegfiltnew(EEG, 'locutoff',0.1,'plotfreqz',0);
    EEG = eeg_checkset( EEG );
    
    %notch filter (50Hz) Line Noise
    EEG = pop_eegfiltnew(EEG, 'locutoff',49.5,'hicutoff',50.5,'revfilt',1,'plotfreqz',0);
    EEG = eeg_checkset( EEG );
    %best filter frequencies: 49.7-50.3
    
    %low-pass filtering
    %     EEG = pop_eegfiltnew(EEG, 'hicutoff',30, 'plotfreqz',0);
    %     EEG.setname= strcat(name, '_filt');
    %     EEG = eeg_checkset( EEG );
    
    EEG.setname= strcat(name, '_filt');
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy/filt');
    EEG = eeg_checkset( EEG );
    
end


%%% Filtering Jeans data sets %%% --> filtering good

cd '/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/samp';

eInfo = dir('*.set'); %take the resampled Energy.set files in the samp folder
Jsamp = {eInfo.name};

for i = 1:length(Jsamp)
    
    a = Jsamp{i};
    name = a(1:5);
    
    %high-pass filtering
    EEG = pop_loadset('filename', a,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/samp/');
    EEG = pop_eegfiltnew(EEG, 'locutoff',0.1,'plotfreqz',0);
    EEG = eeg_checkset( EEG );
    
    %notch filter (50Hz) Line Noise
    EEG = pop_eegfiltnew(EEG, 'locutoff',49.5,'hicutoff',50.5,'revfilt',1,'plotfreqz',0);
    EEG = eeg_checkset( EEG );
    
    %low-pass filtering
    %     EEG = pop_eegfiltnew(EEG, 'hicutoff',30, 'plotfreqz',0);
    %     EEG.setname= strcat(name, '_filt');
    %     EEG = eeg_checkset( EEG );
    
    EEG.setname= strcat(name, '_filt');
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/filt');
    EEG = eeg_checkset( EEG );
    
end


b = struct('c', {'/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/filt/', '/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy/filt/','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC/filt/', '/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO/filt/'});

for i = 1:length(b)
    
    d = b(i).c
    cd (d);
    
    icaInfo = dir('*.set'); %take filtered files from filt folder
    icaDat = {icaInfo.name};

    for j = 1:length(icaDat)
        
        a = icaDat{j};
        name = a(1:5);
        
    EEG = pop_loadset('filename', a,'filepath', d);
    EEG = eeg_checkset( EEG );
    EEG = pop_runica(EEG, 'icatype', 'runica', 'extended',1,'interrupt','on');
    EEG = eeg_checkset( EEG );
    
    EEG.setname= strcat(name, '_ica');
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/ICA');
    EEG = eeg_checkset( EEG );

    end
end


%todo: test if it's better to do a low-,high- and notch- filter or
%low-, high- and notch



%% ICA

b = struct('c', {'/Users/amycheung/Desktop/Masterarbeit/audiofiles/Jeans/filt/', '/Users/amycheung/Desktop/Masterarbeit/audiofiles/Energy/filt/','/Users/amycheung/Desktop/Masterarbeit/audiofiles/EC/filt/', '/Users/amycheung/Desktop/Masterarbeit/audiofiles/EO/filt/'});

for i = 1:length(b)
    
    d = b(i).c
    cd (d);
    
    icaInfo = dir('*.set'); %take filtered files from filt folder
    icaDat = {icaInfo.name};

    for j = 1:length(icaDat)
        
        a = icaDat{j};
        name = a(1:5);
        
    EEG = pop_loadset('filename', a,'filepath', d);
    EEG = eeg_checkset( EEG );
    EEG = pop_runica(EEG, 'icatype', 'runica', 'extended',1,'interrupt','on');
    EEG = eeg_checkset( EEG );
    
    EEG.setname= strcat(name, '_ica');
    EEG = pop_saveset( EEG, 'filename', EEG.setname ,'filepath','/Users/amycheung/Desktop/Masterarbeit/audiofiles/ICA');
    EEG = eeg_checkset( EEG );

    end
end


%% Epoching finished ICA

EEG.test = 'MAMEO_ica.set';

EEGout = eeg_regepochs(EEG.test, 'recurrence', 2, 'rmbase', NaN);

a = 'MAMEO_ica.set'
EEG = pop_loadset('filename', a,'filepath', '/Users/amycheung/Desktop/Masterarbeit/audiofiles/ICA');
EEG = eeg_regepochs(EEG, 'recurrence', 8, 'limits', [0 8], 'rmbase', NaN);
%Variable EEG has to be a struct
%help eeg_regepochs